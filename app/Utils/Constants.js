
const DEFAULT_RECORD_ID = '0';
const DEFAULT_RECORD_COUNT = '10';


const statusCodeModel = {};
statusCodeModel.DEFAULT_RECORD_ID = DEFAULT_RECORD_ID;
statusCodeModel.DEFAULT_RECORD_COUNT = DEFAULT_RECORD_COUNT;

module.exports = statusCodeModel;