const statusCode = require('../config/statusCode.js');

exports.formatDateTime = (date) =>{

    var d = date,
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(); 

   var hrs  = ''+ d.getHours(),
   mins     = ''+ d.getMinutes(),
   sec      = ''+ d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day     = '0' + day;

    if (hrs.length < 2) hrs     = '0' + hrs;
    if (mins.length < 2) mins   = '0' + mins;
    if (sec.length < 2) sec     = '0' + sec;


    var dDate =  [year, month, day].join('-');

    var dTime =  [hrs, mins, sec].join('-');

    return  dDate +' '+ dTime;
}

Number.prototype.padLeft = function(base,chr){
    var  len = (String(base || 10).length - String(this).length)+1;
    return len > 0? new Array(len).join(chr || '0')+this : this;
}

const isDebug = true;

exports.showLog =(message, error, res) =>{

    console.error(error.message);

    if(isDebug){
        return res.status(statusCode.paramsNotFound.code).send(error);
    }
    else{
        return res.status(statusCode.paramsNotFound.code).send(message);
    }
} 