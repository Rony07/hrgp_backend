const env = require('./env.js');
let mysql = require('mysql');



var db = mysql.createPool({
  connectionLimit: env.pool.max,
  host: env.host,
  user: env.username,
  password: env.password,
  database: env.database
});
 

module.exports = db;