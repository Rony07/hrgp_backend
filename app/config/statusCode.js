const statusCode = {
    userAlreadyExists:{
        code: 449,
        message: {error:'User already exists!', message:'The email is already registered with another user!'}
    },
    userRegistered:{
        code: 201,
        message: {success:'User registered!', message:'User is registered successfully!'}
    },
    paramsNotFound:{
        code: 400,
        message: {error:"Params not found", message:"Some of the required params were not passed to this script!"}
        
    },
    invalidUser:{
        code: 400,
        message: {error:"Does not match!", message:"Username or password does not match the records!"}
    },
    genericSuccess:{
        code: 200,
        message: {success:"Generic Success", message:"Generic success message!"}
    },
    noTokenProvided:{
        code: 403,
        message: {error:"No token", message:"No token provided to script!",auth: false}
    },
    tokenVerficationFailed:{
        code: 500,
        message: {error:"Failed Authentication", message:"Unable to verify token!", auth:false}
    },
    genericError:{
        code: 404,
        message: {error:"Something went wrong", message:"Something went wrong", auth:false}
    },
    userNotFound:{
        code: 404,
        message: {error:"User Not Found", message:"The email has not been authorized for any account!", auth:false}
    },
    otpError:{
        code: 400,
        message: {error:"Otp Not Matched", message:"Sorry the otp does not match our records!"}
    }

};

module.exports = statusCode;
