const db = require('../config/db.config.js');
const config = require('../config/config.js');
const statusCode = require('../config/statusCode.js');
const wallPostModel =  require('../models/company/WallPostModel.js');


exports.jobsearch = (data,res) => {

	db.getConnection(function (err, connection) {
		if (err) {
			callback(true);
			return;
		}


		var jobTitle = '%'+data.body.keyword+'%';
		let sql_query = 'SELECT * FROM hrgp.company_jobs where (title like ? or job_desc like ?)';
        var payload = {};


		connection.query(sql_query, [jobTitle, jobTitle], (error, results) => {
			if (error) {
				return console.error(error.message);
			}

			payload.details = results;
            return res.status(statusCode.genericSuccess.code).send(wallPostModel.getSearchResult(payload));

		})

		connection.release();

			connection.on('error', function (err) {
				callback(true);
				return;
			});
	});
}
