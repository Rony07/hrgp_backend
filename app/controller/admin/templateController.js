const db = require('../../config/db.config');
const config = require('../../config/config.js');
const statusCode = require('../../config/statusCode.js');
const authJwt = require('../../router/verifyJwtToken');
var request = require("request");

var bcrypt = require('bcryptjs');

var template_data = [];


exports.pushEvents = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var user_id = new Date().getTime();
        var params = [];

        let sql = 'CALL get_admin_push_categories';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(JSON.stringify(results[0]));
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });

}


exports.addTemplate = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var body = req.body;
        var params = [body.event_id, body.email_subject, body.email_content, body.sms_template, body.push_template];

        let sql = 'CALL add_admin_push_template(?,?,?,?,?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(JSON.stringify(results[0][0]));
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.editTemplate = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var body = req.body;
        var params = [body.event_id, body.email_subject, body.email_content, body.sms_template, body.push_template, body.id];

        let sql = 'CALL edit_admin_push_template(?,?,?,?,?,?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess);
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.getTemplates = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var params = [];

        let sql = 'CALL push_template_detailed()';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(JSON.stringify(results[0]));
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.changeTemplateActive = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var body = req.body;
        var params = [body.event_id, body.id, body.active];

        let sql = 'CALL update_admin_template_active(?,?,?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess);
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.deleteTemplate = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var body = req.body;
        var params = [body.id];

        let sql = 'CALL delete_template_admin(?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess);
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

