const db = require('../../config/db.config');
const config = require('../../config/config.js');
const statusCode = require('../../config/statusCode.js');
const authJwt = require('../../router/verifyJwtToken');
var request = require("request");

var bcrypt = require('bcryptjs');

exports.getUsers = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var body = req.body;
        var params = [body.filter, body.user_id, body.result_count];

        let sql = 'CALL get_admin_users(?,?,?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            return res.status(statusCode.genericSuccess.code).send(JSON.stringify(results[0]));
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });

}

