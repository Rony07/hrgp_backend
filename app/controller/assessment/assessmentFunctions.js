const request = require("request");
const rebrandly = require('../../config/rebrandly');
const db = require('../../config/db.config.js');
const config = require('../../config/config.js');

exports.process_job_step = (jobID, candidateID, guidedHiringID, currentStep) => {


}

exports.initialInsert = (jobID, candidateID) => {
    var formBuilderId = [70];
    let getFormBuilder = 'CALL getGhFormByID(?)';



    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }


        connection.query(getFormBuilder, formBuilderId, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            console.log(results[0][0]);
            generate_rebrand_link("http://www.rohangirdhani.com");

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });



    function generate_rebrand_link(original_link) {
        var options = {
            method: 'POST',
            url: 'https://api.rebrandly.com/v1/links',
            headers:
            {
                'Postman-Token': '3cb35855-6752-46ad-bb98-a0bed20d4b28',
                'cache-control': 'no-cache',
                workspace: '4f0438d122f54b0eab2f537b18248298',
                apikey: 'c631043c06dd442499d6a36ac49d11de',
                'Content-Type': 'application/json'
            },
            body:
            {
                destination: original_link,
                domain: { fullName: 'rebrand.ly' }
            },
            json: true
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log(body.shortUrl);
        });

    }
}