const db = require('../config/db.config.js');
const config = require('../config/config.js');
const statusCode = require('../config/statusCode.js');
const authJwt = require('../router/verifyJwtToken');
const candidateModel = require('../models/candidate_model');

var candidate_details = {};


exports.getprofile = (req, res) => {
    get_candidate_details(req.body.user_id, res);
}


exports.edit = (req, res) => {

    var qualifications = req.body.qualifications;
    var skills = req.body.skills;
    var address = req.body.address;
    var job_expierence = req.body.job_expierence;
    var certifications = req.body.certifications;
    var preferred_location = req.body.preferred_location;



    var user_id = req.body.user_id;
    var data = req.body;
    var current_address = address.current_address;
    var payload = {
        current_address: 0,
        permanent_address: 0
    };
    var current_params = [current_address.address1, current_address.address2, current_address.city,
        current_address.state, current_address.pincode, current_address.country, current_address.geo_id
    ];
    var permanent_address = address.permanent_address;
    var permanent_params = [permanent_address.address1, permanent_address.address2, permanent_address.city,
        permanent_address.state, permanent_address.pincode, permanent_address.country, permanent_address.geo_id
    ];


    add_skills(user_id, skills);
    add_qualification(user_id, qualifications);
    add_expierence(user_id, job_expierence);
    add_certification(user_id, certifications);
    add_preferedlocation(user_id,preferred_location);



    let address_sql = 'CALL add_address(?,?,?,?,?,?,?)';



    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }


        connection.query(address_sql, current_params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            payload.current_address = results[0][0].insert_id;

            connection.query(address_sql, permanent_params, (error, results) => {
                if (error) {
                    return console.error(error.message);
                }
                payload.permanent_address = results[0][0].insert_id;
            });

            connection.query(address_sql, permanent_params, (error, results) => {
                if (error) {
                    return console.error(error.message);
                }
                payload.permanent_address = results[0][0].insert_id;
                var edit_params = [user_id, data.firstname, data.lastname, data.image, data.image2, payload.current_address, payload.permanent_address,
                    data.about_me
                ]
                edit_candidate(edit_params, res);

            });

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


get_candidate_details = (user_id, res) => {

    let get_skills = 'CALL get_skills(?)';
    let get_qualifications = 'CALL get_candidate_qualification(?)';
    let get_expierence = 'CALL get_candidate_expierence(?)';
    let get_certification = 'CALL get_candidate_certification(?)';
    let get_preferedLocation = 'CALL get_prefered_location(?)';


    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(get_skills, user_id, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            candidate_details.skills = results[0];


            connection.query(get_qualifications, user_id, (error, results) => {
                if (error) {
                    return console.error(error.message);
                }
                candidate_details.qualifications = results[0];


                connection.query(get_expierence, user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }
                    candidate_details.expierence = results[0];

                    connection.query(get_certification, user_id, (error, results) => {
                        if (error) {
                            return console.error(error.message);
                        }
                        candidate_details.certification = results[0];

                        connection.query(get_preferedLocation, user_id, (error, results) => {
                            if (error) {
                                return console.error(error.message);
                            }
                            candidate_details.preferred_location = results[0];
    
                            return_candidate_details(user_id, res);
                        });
                    });
                });
            });
        });


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


function edit_candidate(data, res) {
    let edit_sql = 'CALL edit_candidate(?,?,?,?,?,?,?,?)';

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(edit_sql, data, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            get_candidate_details(data[0], res);
        });


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

function add_skills(user_id, data) {

    var values = [];
    var master_values = [];
    var i = 1;
    var len = data.length;
    data.forEach(element => {
        var temp = [user_id, element, 'candidate_skills', user_id];
        values.push(temp);
        master_values.push([element]);

        if (i == len) {

            db.getConnection(function (err, connection) {
                if (err) {
                    callback(true);
                    return;
                }


                connection.query("DELETE FROM common_skills WHERE type_id = ?", user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }


                    connection.query("INSERT INTO common_skills (user_id, name, type, type_id) VALUES ?", [values], (error, results) => {
                        if (error) {
                            return console.error(error.message);
                        }


                        connection.query("INSERT INTO master_skills (skill) VALUES ?", [master_values], (error, results) => {
                            if (error) {
                                return console.error(error.message);
                            }
                        });
                    });
                });

                connection.release();
                connection.on('error', function (err) {
                    callback(true);
                    return;
                });
            });
        }
        i++;
    });
}

function add_qualification(user_id, data) {
    var values = [];
    var i = 1;
    var len = data.length;
    data.forEach(element => {
        var temp = [user_id, element.type, element.name, element.from_date, element.to_date];
        values.push(temp);
        if (i == len) {

            db.getConnection(function (err, connection) {
                if (err) {
                    callback(true);
                    return;
                }

                connection.query("DELETE FROM candidate_qualifications WHERE user_id = ?", user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }


                    connection.query("INSERT INTO candidate_qualifications (user_id, type, name, from_date, to_date) VALUES ?", [values], (error, results) => {
                        if (error) {
                            return console.error(error.message);
                        }
                    });
                });


                connection.release();

                connection.on('error', function (err) {
                    callback(true);
                    return;
                });
            });
        }
        i++;
    });
}

function add_expierence(user_id, data) {
    var values = [];
    var master_values = [];
    var i = 1;
    var len = data.length;
    data.forEach(element => {
        var temp = [user_id, element.company_name, element.from_date, element.to_date, element.present, element.designation];
        master_values.push([element.company_name]);
        values.push(temp);
        if (i == len) {

            db.getConnection(function (err, connection) {
                if (err) {
                    callback(true);
                    return;
                }

                connection.query("DELETE FROM candidate_job_expierence WHERE user_id = ?", user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }

                    
                    connection.query("INSERT INTO candidate_job_expierence (user_id, company_name, from_date, to_date," +
                        "present, designation) VALUES ?", [values], (error, results) => {

                            if (error) {
                                return console.error(error.message);
                            }

                            connection.query("INSERT INTO master_company (company_name) VALUES ?", [master_values], (error, results) => {
                                if (error) {
                                    return console.error(error.message);
                                }
                            });

                        });
                });


                connection.release();

                connection.on('error', function (err) {
                    callback(true);
                    return;
                });
            });
        }
        i++;
    });
}

function add_certification(user_id, data) {
    var values = [];
    var master_values = [];
    var i = 1;
    var len = data.length;
    data.forEach(element => {
        var temp = [user_id, element.name, element.from_date, element.to_date, "0", element.provider];
        let temp2 = [element.name];
        master_values.push(temp2);
        values.push(temp);
        if (i == len) {

            db.getConnection(function (err, connection) {
                if (err) {
                    callback(true);
                    return;
                }

                connection.query("DELETE FROM candidate_certifications WHERE user_id = ?", user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }


                    connection.query("INSERT INTO candidate_certifications (user_id, name, from_date, to_date," +
                        "number, provider) VALUES ?", [values], (error, results) => {

                            connection.query("INSERT INTO master_certificates (certificate_name) VALUES ?", [master_values], (error, results) => {
                                if (error) {
                                   console.error(error.message);
                                }
                            });

                            if (error) {
                                console.error(error.message);
                            }
                        });
                });


                connection.release();

                connection.on('error', function (err) {
                    callback(true);
                    return;
                });
            });
        }
        i++;
    });
}

function add_preferedlocation(user_id, data) {
    var values = [];
    var i = 1;
    var len = data.length;
    data.forEach(element => {
        var temp = [user_id, element.country, element.state, element.city];
        values.push(temp);
        if (i == len) {

            db.getConnection(function (err, connection) {
                if (err) {
                    callback(true);
                    return;
                }

                connection.query("DELETE FROM prefered_job_location WHERE user_id = ?", user_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }


                    connection.query("INSERT INTO prefered_job_location (user_id, country, state, city) VALUES ?", [values], (error, results) => {


                            if (error) {
                                console.error(error.message);
                            }
                        });
                });


                connection.release();

                connection.on('error', function (err) {
                    callback(true);
                    return;
                });
            });
        }
        i++;
    });
}


function return_candidate_details(user_id, res) {
    let candidate_sql = 'CALL candidate_details(?)';

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(candidate_sql, user_id, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            candidate_details.details = results[0][0];
            return res.status(statusCode.genericSuccess.code).send(candidateModel.candidate(candidate_details));
        });


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}
