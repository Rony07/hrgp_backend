var request = require("request");

exports.send_message = (email, subject, message) => {
    var options = { method: 'POST',
    url: 'http://18.191.220.132/mail/send.php',
    headers: 
     { 'Postman-Token': '76166407-18a3-4bf0-bb2a-a8d9f81611e2',
       'cache-control': 'no-cache',
       'Content-Type': 'application/json' },
    body: 
     { email: email,
       subject: subject,
       name: 'name',
       message: message },
    json: true };
  
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
  
    console.log(body);
  });
}