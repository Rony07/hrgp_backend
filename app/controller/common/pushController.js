var request = require("request");

exports.send_push = (token, message, delay) => {
    setTimeout(() => {
        var options = {
            method: 'POST',
            url: 'https://fcm.googleapis.com/fcm/send',
            headers:
            {
                'Postman-Token': 'd065e8a2-f874-4b62-a8ea-4967b133c0a3',
                'cache-control': 'no-cache',
                Authorization: 'key=AAAAOYyrUEY:APA91bGpHytacIkZbtLk3P7oTZUGb2HKw35KNzLmnNBwWJDHte-20wyz8vqMP_fQltAP6h2VXLKlVFidCLOXdrGmlNzyOppAWYQTH1I0YOiGAOWYNZ8CswkseTL1snXfmhU8sXmZgamd',
                'Content-Type': 'application/json'
            },
            body:
            {
                notification:
                {
                    title: 'HRGP',
                    body: message,
                    click_action: 'http://localhost:8000/',
                    icon: 'https://www.hrgp.co/include/images/HRGP_logo_257.png'
                },
                to: token
            },
            json: true
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            // console.log(token, message);
        });
    }, delay);

}
