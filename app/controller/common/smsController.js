const twilio_secret = require('../../config/twilio');
const client = require('twilio')(twilio_secret.account_sid, twilio_secret.auth_token);

exports.send_message = (phoneno, message) => {
    client.messages
    .create({
       body: message,
       from: twilio_secret.phone_no,
       to: phoneno
     })
    .then(message => console.log(message.sid));
}