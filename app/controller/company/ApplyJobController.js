const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');
const applyJobModel = require('../../models/company/ApplyJobModel.js')

exports.applyJob = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }
        else{

        var form_id =     req.query.form_id;
        var job_id =     req.query.job_id;

        var params = [form_id,'1'];
        let call_procedure = 'CALL get_questioner_data(?,?)'


        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                appUtils.showLog(statusCode.genericError.code, error, res)

            } else {

                    return res.status(statusCode.genericSuccess.code).send(applyJobModel.getQuestionData(results[0]));
                }
            });
        }
    });
}

