const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const assesmentmodel = require('../../models/assesment_model.js');


exports.createAssesment = (req, res) => {
    create_assesment(req.body, res);
}

create_assesment = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = body;
        var params = [body.user_id, data.title, data.desc, data.image, data.isTimer,
        new Date(), data.active, data.job_skills, data.visible_question, data.main_category_id, data.sub_category_id];


        let call_procedure = 'CALL create_assesment_main(?,?,?,?,?,?,?,?,?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            if(results[0][0].insert_id != undefined){


                var postData = {};
                postData.message = "Successfully created the assesment.";
                postData.insert_id = results[0][0].insert_id;
            
                return res.status(statusCode.genericSuccess.code).send(postData);
            
            }
            else{
                return res.status(statusCode.genericError.code).send(statusCode.genericError.message);

            }
        })

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.editAssesment = (req, res) => {


    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = req.body;
        var params = [data.user_id, data.title, data.desc, data.image, data.isTimer, new Date(), 
                      data.active, data.job_skills, data.visible_question, data.assesment_id,data.main_category_id, 
                      data.sub_category_id];


        let call_procedure = 'CALL edit_assesment_main(?,?,?,?,?,?,?,?,?,?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            if(results[0][0].false == undefined){

                payload.details = results[0];
                return res.status(statusCode.genericSuccess.code).send(assesmentmodel.success(payload));
            }
            else{
                return res.status(statusCode.genericError.code).send(statusCode.genericError.message);

            }
        })

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.getAssesment = (req, res) => {
    get_assesment(req.body.user_id, res);
}

get_assesment = (company_id, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        let call_procedure = 'CALL get_company_assesment(?)';
        var payload = {};


        connection.query(call_procedure, company_id, (error, results) => {

            if (error) {
                return console.error(error.message);
            }

            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(assesmentmodel.assesment_main(payload));

        })


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.createAssesmentQue = (req, res) => {
    create_assesment_que(req.body, res);
}

create_assesment_que = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = body;
        var params = [data.user_id, data.assesment_test_id, data.que_title, data.que_media_type, data.ans_media_type, data.ans_option, JSON.stringify(data.ans_data) ,
        new Date(), data.active];


        let call_procedure = 'CALL create_assesment_data(?,?,?,?,?,?,?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(assesmentmodel.success(payload));

        })


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.editAssesmentQue = (req, res) => {
    edit_assesment_que(req.body, res);
}

edit_assesment_que = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = body;
        var params = [data.user_id,  data.que_id,data.assesment_id, data.que_title, data.que_media_type, data.ans_media_type, 
            data.ans_option, data.ans_data,
        new Date(), data.active];


        let call_procedure = 'CALL edit_assesment_data(?,?,?,?,?,?,?,?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(assesmentmodel.success(payload));

        })


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.editQuestionOrder = (req, res) => {
    edit_Question_Order(req.body, res);
}

edit_Question_Order = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = body;
        var params = [data.user_id,  data.assesment_id, JSON.stringify(data.que_order_info) ];

        let call_procedure = 'CALL assesment_change_order(?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(assesmentmodel.success(payload));
        })


        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}



exports.getAssesmentdata = (req, res) => {
    get_assesment_data(req.body.assesment_id, res);
}

get_assesment_data = (assesment_id, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        let call_procedure = 'CALL get_assesment_data(?)';
        var payload = {};


        connection.query(call_procedure, assesment_id, (error, results) => {

            if (error) {
                return console.error(error.message);
            }

            payload.details = results[0];

            var select_Query = "Select question_order_info from  assesment_main where id = "+"'"+assesment_id +"'";

            connection.query(select_Query, (error1, results1) => {

                if (error1) {

                    console.error(error1.message);
                    return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
                }

                return res.status(statusCode.genericSuccess.code).send(assesmentmodel.assesment_data(payload));

            })
        })


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.deleteAssesmentMain = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var que_id = req.query.que_id;
        
        connection.query("Delete from assesment_data where id = ?", que_id, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            var postData = {};
            postData.message = "Successfully Deleted";

            return res.status(statusCode.genericSuccess.code).send(postData);
        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });
}

exports.deleteQuestion = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var que_id = req.query.que_id;

        connection.query("Delete from assesment_data where id = ?", que_id, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            var postData = {};
            postData.message = "Successfully Deleted";

            return res.status(statusCode.genericSuccess.code).send(postData);
        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });
}
