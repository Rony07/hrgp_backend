const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const employeeModel = require('../../models/company/EmployeeModel.js')


exports.addEmployee = (req, res) => {
    addEmployee(req.body, res);
}

addEmployee = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = body;
        var params = [  data.employee_company_id,   body.user_id,               data.first_name, data.last_name,    data.email, 
                        data.phone_num,             data.date_of_birth,         data.experience, data.designation, data.location,
                        data.emp_image,             data.emp_description,       data.image,      data.isTimer,
                      /* data.current_address_id, data.permanent_address_id */
                       '348',                       '348',                      data.job_start_date, data.job_end_date, data.job_type, 
                       data.job_location, data.active];
                       


        let call_procedure = 'CALL add_company_user(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            if(results[0][0].false == undefined){

                payload.details = results[0];

                var postData = {};
                postData.message = "Successfully added new employee.";

                return res.status(statusCode.genericSuccess.code).send(postData);
            }
            else{

                return res.status(statusCode.genericError.code).send(statusCode.genericError.message);
            }
        })

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.editEmployee = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = body;
        var params =    [data.emp_id,   data.employee_company_id,   body.user_id,               data.first_name, data.last_name,    data.email, 
                                        data.phone_num,             data.date_of_birth,         data.experience, data.designation, data.location,
                                        data.emp_image,             data.emp_description,       data.image,      data.isTimer,
                                        /* data.current_address_id, data.permanent_address_id */
                                        '348',                       '348',                      data.job_start_date, data.job_end_date, data.job_type, 
                                        data.job_location, data.active];
                       


        let call_procedure = 'CALL edit_company_user(?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            if(results[0][0].false == undefined){

                payload.details = results[0];

                var postData = {};
                postData.message = "Successfully edited new employee.";

                return res.status(statusCode.genericSuccess.code).send(postData);
            }
            else{

                return res.status(statusCode.genericError.code).send(statusCode.genericError.message);
            }
        })

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.getEmployees = (req, res) => {


    var company_id =  req.body.user_id

    var recordID = parseInt(req.query.recordID) ;  
    var count = parseInt(req.query.count) ;
    

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var params = [company_id, recordID, count];

        let call_procedure = 'CALL get_company_employee(?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                return console.error(error.message);
            }
            var payload = {};
            payload.details = results[0];
            payload.last_rec_id = (recordID + count);
            payload.totalcount = results[1][0].total_count;


            
           // return res.status(statusCode.genericSuccess.code).send(JSON.stringify(results[0]));
            return res.status(statusCode.genericSuccess.code).send(employeeModel.getAllEmployees(payload));
        });
        connection.release();
        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });

}
