const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');
const companyModel = require('../../models/company_model.js');

exports.followCompany = (req, res) => {
    followCompany(req.body, res);
}

followCompany = (data, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res);
        }

        let sql_query = 'insert into company_followers (user_id, company_id) VALUES (' + "'" + data.user_id + "'" + ' ,  ' + "'" + body.follow_company_id + "'" + ')';

        connection.query(sql_query, params, (error, results) => {

            if (error) {

                appUtils.showLog(statusCode.genericError.code, error, res);
            }
            else{

                return res.status(statusCode.genericError.code).send(statusCode.genericError.message);
            }

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.unfollowCompany = (req, res) => {
    unfollow_Company(req, res);
}

unfollow_Company = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res);
        }

        let company_id = req.query.company_id
        //req.body.user_id = '1550747390779';

        let sql_query = 'Delete from company_followers where user_id = '+"'" + req.body.user_id + "'" +' and  company_id = '+"'" + company_id + "'" ;

        connection.query(sql_query, (error, results) => {

            if (error) {

                appUtils.showLog(statusCode.genericError.code, error, res);
            }
            else{

                var postData = {};
                postData.message = "Successfully Deleted";

                return res.status(statusCode.genericError.code).send(postData);
            }

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}


exports.getfollowList = (req, res) => {
    get_followList(req.body, res);
}

get_followList = (data, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res);
        }

        let sql_query = 'select company_followers.company_id, company_data.company_name, company_data.image,'+ 
                        'company_data.short_desc, company_data.website, company_data.company_size, company_data.followers '+
                        'from company_followers '+ 
                        'left join company_data ON company_followers.company_id = company_data.company_id '+
                        'where user_id = '+"'"+data.user_id+"'";

        connection.query(sql_query, (error, results) => {

            if (error) {

                appUtils.showLog(statusCode.genericError.code, error, res);
            }
            else{

                let payload = {};
                payload.details = results;

                return res.status(statusCode.genericSuccess.code).send(companyModel.getCompanyData(payload));

            }

        });

        connection.release();

    });
}

