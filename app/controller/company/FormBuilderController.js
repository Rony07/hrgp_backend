const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const formBuilderModel = require('../../models/company/FormBuilder_Model.js');
const appUtils = require('../../Utils/Utils.js');


exports.createFormBuilder = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        form_builder_data_validation(dataObj);
        // create_formBuilder_asynch(dataObj);
    });
}

const form_builder_data_validation = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;
    let req = dataObj.req;
    let form_user_data = dataObj.req.body.form_data.form_info;

    const getPromis1 = new Promise(function (resolve, reject) {

        company_id = '1554447902763';

        var params = [company_id];
        let call_procedure = 'CALL get_Assesment_Category(?)'


        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results;
                metaObj.company_id = company_id;

                resolve(metaObj);
            }
        });
    });


    let waitPromis1 = await getPromis1.then(function (resultData) {

        assesment_category2(resultData).then(function (response) {

            if (isFormDataValid(mapToObj2(response), form_user_data)) {

                console.log('Form Data Valid');

                //resolve('Data is Correct');
                if (req.body.form_id) {

                    editFormBuilder_async(req.body, res, connection);

                } else {

                    create_formBuilder_async(req.body, res, connection);
                }


            } else {

                console.log('Form Data IN Valid');
                connection.release();
                var message = {};
                message.data = 'Getting parameters values wrong.'
                return res.status(statusCode.genericSuccess.code).send(message);
            }

        }).catch(function (error) {

            connection.release();
            appUtils.showLog(statusCode.genericError.code, error, res)

        });
    });
}

const isFormDataValid = (form_org_data, form_data_user) => {

    var isFormDataValid = true;

    if (form_data_user) {

        var isFormDataValidArray = [];

        for (var i = 0; i < form_data_user.length; i++) {

            var element = form_data_user[i];

            if (isExist(element.que_set_id, element.main_category_id, form_org_data)) {

                isFormDataValidArray[i] = true;

            } else {
                isFormDataValid = false
                isFormDataValidArray[i] = false;
            }
        }
        console.log(isFormDataValidArray);

    } else {
        isFormDataValid = false
    }


    return isFormDataValid;

}

const isExist = (que_set_id, main_category_id, form_org_data) => {

    var isExist = false;

    for (var i = 0; i < form_org_data.dataArray.length; i++) {

        var elementArray = form_org_data.dataArray[i].que_data;

        for (var j = 0; j < elementArray.length; j++) {

            var element1 = elementArray[j];

            if (element1.que_set_id == que_set_id) {

                isExist = true;
                break;
            }
        }
    }

    return isExist;

    /* form_org_data.fo
    if(element.que_set_id) */
}

const create_formBuilder_async = async (body, res, connection) => {


    const getPromis1 = new Promise(function (resolve, reject) {

        var data = body;
        var params = [body.user_id, data.form_text_id, data.form_title, '', data.company_position,
            appUtils.formatDateTime(new Date()), appUtils.formatDateTime(new Date()), data.active
        ];


        let call_procedure = 'CALL create_gh_form(?,?,?,?,?,?,?,?)';

        let form_data_info = data.form_data.form_info;

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let data_values = [];

                form_data_info.forEach(element => {

                    if (element.sub_category_info.length > 0) {

                        element.sub_category_info.forEach(element2 => {

                            var temp = [results[0][0].insert_id, element.main_category_id, element2.sub_category_id, element.que_set_id, element.main_category_order_key];
                            data_values.push(temp);
                        });
                    } else {

                        var temp = [results[0][0].insert_id, element.main_category_id, , element.que_set_id, element.main_category_order_key];
                        data_values.push(temp);
                    }
                })


                connection.query("DELETE FROM form_builder_data WHERE form_id = ?", params[0], (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }
                    connection.query("INSERT INTO form_builder_data (form_id, main_category_id, sub_category_id, que_set_id, order_key) VALUES ?", [data_values], (error, results) => {

                        if (error) {

                            reject(error);
                        } else {

                            let metaObj = {};
                            metaObj.connection = connection;
                            metaObj.details = results[0];

                            resolve(metaObj);
                        }
                    });
                });
            }
        });
    });

    let waitPromis1 = await getPromis1.then(function (payload) {


        var postData = {};
        postData.message = "Successfully created guided hiring form.";
        //postData.recordId = payload.details;

        return res.status(statusCode.genericSuccess.code).send(postData);

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

const editFormBuilder_async = async (body, res, connection) => {


    const getPromis1 = new Promise(function (resolve, reject) {

        var data = body;
        var params = [data.form_id, data.user_id, data.form_text_id, data.form_title, '', data.company_position,
            appUtils.formatDateTime(new Date()), data.active
        ];


        let call_procedure = 'CALL edit_gh_form(?,?,?,?,?,?,?,?)';
        var payload = {};

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let form_data_info = data.form_data.form_info;
                let data_values = [];

                form_data_info.forEach(element => {

                    if (element.sub_category_info.length > 0) {

                        element.sub_category_info.forEach(element2 => {

                            var temp = [data.form_id, element.main_category_id, element2.sub_category_id, element.que_set_id, element.main_category_order_key];
                            data_values.push(temp);
                        });
                    } else {

                        var temp = [data.form_id, element.main_category_id, , element.que_set_id, element.main_category_order_key];
                        data_values.push(temp);
                    }
                })


                connection.query("DELETE FROM form_builder_data WHERE form_id = ?", params[0], (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }
                    connection.query("INSERT INTO form_builder_data (form_id, main_category_id, sub_category_id, que_set_id, order_key) VALUES ?", [data_values], (error, results) => {

                        if (error) {

                            reject(error);
                        } else {

                            let metaObj = {};
                            metaObj.connection = connection;
                            metaObj.details = results[0];

                            resolve(metaObj);
                        }
                    });
                });

            }
        });
    });

    let waitPromis1 = await getPromis1.then(function (payload) {

        var postData = {};
        postData.message = "Successfully edited guided hiring form.";
        //postData.recordId = payload.details;


        return res.status(statusCode.genericSuccess.code).send(postData);

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}


exports.editFormBuilder = (req, res) => {


    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        form_builder_data_validation(dataObj);
    });
}

exports.getAllForm = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res);

        } else {

            let company_id = req.body.user_id;

            let sql_query = 'Select form_title, form_id From form_builder Where company_id = ' + "'" + company_id + "'" + ' AND  isDeleted = 0';

            connection.query(sql_query, (error, results) => {

                if (error) {

                    appUtils.showLog(statusCode.genericError.code, error, res);

                } else {

                    var response = {};
                    response.data = [];

                    results.forEach(element => {

                        var postData = {};

                        postData.form_id = element.hasOwnProperty("form_id") ? element.form_id : "";
                      //  postData.form_text_id = element.hasOwnProperty("form_text_id") ? element.form_text_id : "";
                        postData.form_title = element.hasOwnProperty("form_title") ? element.form_title : "";
                        response.data.push(postData);
                    })

                    return res.status(statusCode.genericSuccess.code).send(response);
                }
            });
        }

    });
}

exports.getFormBuilder = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        } else {

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            get_FormBuilder_asynch(dataObj);
        }

    });
}

get_FormBuilder_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;

    let call_procedure = 'CALL get_gh_form(?)';
    var payload = {};

    const getPromis1 = new Promise(function (resolve, reject) {

        connection.query(call_procedure, company_id, (error, results) => {

            if (error) {

                reject(error);
            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results[0];
                metaObj.company_id = company_id;

                resolve(metaObj);
            }
        });

    });

    let waitPromis1 = await getPromis1.then(function (resultData) {

        getform_builder_main(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        })

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });

}

exports.getFormBuilderID = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        } else {

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            get_FormBuilderID_asynch(dataObj);
        }

    });

}

get_FormBuilderID_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;
    var form_id = dataObj.req.query.form_id;

    var params = [company_id, form_id];


    const getPromis1 = new Promise(function (resolve, reject) {


        let call_procedure = 'CALL get_gh_form_id(?,?)';
        var payload = {};


        connection.query(call_procedure, params, (error, results) => {

            if (error) {
                return console.error(error.message);
            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results[0];
                metaObj.company_id = company_id;

                resolve(metaObj);
            }
        })
    });

    let waitPromis1 = await getPromis1.then(function (resultData) {

        getform_builder_main(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        })

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });

}

const getform_builder_main = (payload, connection) => {


    return new Promise(function (resolve, reject) {

        var response = {};
        response.data = [];
        var pqueue_promis = [];

        payload.details.forEach(element => {

            var postData = {};


            postData.form_id = element.hasOwnProperty("form_id") ? element.form_id : "";
            postData.form_text_id = element.hasOwnProperty("form_text_id") ? element.form_text_id : "";
            postData.form_title = element.hasOwnProperty("form_title") ? element.form_title : "";
            // postData.form_data = element.hasOwnProperty("form_data") ? JSON.parse(element.form_data) : "";
            postData.company_position = element.hasOwnProperty("company_position") ? element.company_position : "";

            postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";
            postData.date_modefied = element.hasOwnProperty("date_modefied") ? element.date_modefied : "";

            postData.active = element.hasOwnProperty("active") ? element.active : "";


            pqueue_promis.push(fetchFormData(postData.form_id, connection).then(function (form_data_info) {

                postData.form_data = form_data_info;

            }));

            response.data.push(postData);
        })

        Promise.all(pqueue_promis).then(function () {

            resolve(response);
        })
    });
}

var fetchFormData = function (form_id, connection) {

    return new Promise(function (resolve, reject) {

        let params = [form_id];

        let call_procedure = 'CALL get_form_builder_data(?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {
                return console.error(error.message);
            }

            var infoArray = [];

            results[0].forEach((element1) => {


                let infoObj = {};

                infoObj.main_category_id = element1.hasOwnProperty("main_category_id") ? element1.main_category_id : ""
                infoObj.main_category_name = element1.hasOwnProperty("main_category_name") ? element1.main_category_name : ""
                infoObj.sub_category_id = element1.hasOwnProperty("sub_category_id") ? element1.sub_category_id : ""
                infoObj.sub_category_name = element1.hasOwnProperty("sub_category_name") ? element1.sub_category_name : ""
                infoObj.que_set_id = element1.hasOwnProperty("que_set_id") ? element1.que_set_id : ""
                infoObj.order_key = element1.hasOwnProperty("order_key") ? element1.order_key : ""

                infoArray.push(infoObj);
            });

            resolve(infoArray);
        })
    });
};




exports.getCompanyPosition = (req, res) => {
    get_CompanyPosition(req.body.user_id, res);
}

get_CompanyPosition = (company_id, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        var payload = {};

        var sql_query = "Select * from master_company_positions";

        connection.query(sql_query, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            payload.details = results;
            return res.status(statusCode.genericSuccess.code).send(formBuilderModel.getCompanyPos(payload));
        });

        connection.release();

        connection.on('error', function (err) {

            console.log(err);
            //callback(true);
            return;
        });
    });
}

exports.get_QuestionersCategory = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        get_AssesmentCategory_asynch(dataObj);
    });
}

const get_AssesmentCategory_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;
    let req = dataObj.req;

    const getPromis1 = new Promise(function (resolve, reject) {

        company_id = '1554447902763';

        let params = [company_id];
        let call_procedure = 'CALL get_Assesment_Category(?)'

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results;
                metaObj.company_id = company_id;


                resolve(metaObj);
            }
        });
    });

    let waitPromis1 = await getPromis1.then(function (resultData) {

        assesment_category2(resultData).then(function (response) {

            connection.release();

            return res.status(statusCode.genericSuccess.code).send(mapToObj2(response));

        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

const assesment_category2 = (resultData) => {

    return new Promise(function (resolve, reject) {

        var resultMap = new Map();

        var pqueue = [];
        resultData.details[0].forEach((element, index) => {


            let mainData = {};
            mainData.main_category_name = element.hasOwnProperty("main_category_name") ? element.main_category_name : "";
            mainData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
            mainData.order_num = element.hasOwnProperty("order_num") ? element.order_num : "";


            let subcatInfo = {};
            subcatInfo.sub_category_name = element.hasOwnProperty("sub_category_name") ? element.sub_category_name : "";
            subcatInfo.sub_category_id = element.hasOwnProperty("sub_category_id") ? element.sub_category_id : "";


            pqueue.push(fetchQueSetData(mainData.main_category_id, resultData.company_id, resultData.connection).then(function (que_data) {

                mainData.que_data = que_data;
            }));

            if (resultMap.has(mainData.main_category_id)) {

                let dataObj = resultMap.get(mainData.main_category_id);

                if (subcatInfo.sub_category_id) {

                    dataObj.mainData.sub_category_info.push(subcatInfo);
                }

                resultMap.set(mainData.main_category_id, dataObj);

            } else {

                let dataObj = {};

                dataObj.mainData = mainData;

                dataObj.mainData.sub_category_info = [];

                if (subcatInfo.sub_category_id) {

                    dataObj.mainData.sub_category_info.push(subcatInfo);
                }

                resultMap.set(mainData.main_category_id, dataObj);
            }
        })

        Promise.all(pqueue).then(function () {

            setTimeout(() => resolve(resultMap), 200);
        })
    });
}

function mapToObj2(inputMap) {

    let response = {};
    response.dataArray = [];
    let obj = [];

    inputMap.forEach(function (value, key) {
        obj.push(value.mainData);
    });

    response.dataArray = obj;
    return response;
}

var fetchQueSetData = function (main_category_id, company_id, connection) {

    return new Promise(function (resolve, reject) {

        var sql_query = "Select title, id as que_set_id from assesment_main where main_category_id = " + "'" + main_category_id + "'" + " and company_id = " + "'" + company_id + "'";

        connection.query(sql_query, (error, results1) => {
            if (error) {
                return console.error(error.message);
            }


            let que_data = [];

            results1.forEach((element1) => {

                var que_bean = {}
                que_bean.title = element1.hasOwnProperty("title") ? element1.title : "";
                que_bean.que_set_id = element1.hasOwnProperty("que_set_id") ? element1.que_set_id : "";

                que_data.push(que_bean);
            })

            resolve(que_data);
        });
    });
};

exports.delete_ghf = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        var form_id = req.query.form_id;

        connection.query("Update form_builder set isDeleted = '1' where form_id = ?", form_id, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            var postData = {};
            postData.message = "Successfully Deleted";

            return res.status(statusCode.genericSuccess.code).send(postData);
        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });
}