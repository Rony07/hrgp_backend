
const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');


exports.getGhfmain = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        get_Ghf_main_asynch(dataObj);
    });
}

const get_Ghf_main_asynch = async (dataObj) => {

    let  connection =  dataObj.connection;
    let  company_id =  dataObj.req.body.user_id;
    let  res        =  dataObj.res;

    company_id = '1554447902763';

    const getPromis1 = new Promise(function (resolve, reject) {

        var sql_query = "SELECT main_category_name, company_ghf_main.main_category_id, company_ghf_main.order_num  FROM hrgp.company_ghf_main where company_id = " + "'" + company_id + "'" + " order by company_ghf_main.order_num ";

        connection.query(sql_query, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results;

                resolve(metaObj);
            }
        });
    });


    let waitPromis1 = await getPromis1.then(function (resultData) {

        getSubcategoryData(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

function getSubcategoryData(payload, connection) {

    return new Promise(function (resolve, reject) {

        var response = {};
        response.data = [];

        var pqueue = [];

        payload.details.forEach((element, index) => {

            var postData = {};

            postData.main_category_name = element.hasOwnProperty("main_category_name") ? element.main_category_name : "";
            postData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
            postData.order_num = element.hasOwnProperty("order_num") ? element.order_num : "";


            pqueue.push(fetchSubData(postData.main_category_id, connection).then(function (sub_category_info) {

                postData.sub_category_info = sub_category_info;
                response.data.push(postData);
            }));
        })

        Promise.all(pqueue).then(function () {

            resolve(response);
        })
    });
}

var fetchSubData = function (main_category_id, connection) {

    return new Promise(function (resolve, reject) {

        var sql_query = "SELECT main_category_id, order_num, sub_category_name, sub_category_id  FROM company_ghf_sub where main_category_id = " + "'" + main_category_id + "'"+" AND company_id = '1554447902763' "  + " order by company_ghf_sub.order_num ";

        connection.query(sql_query, (error, results1) => {
            if (error) {
                return console.error(error.message);
            }

            var sub_category_info = [];

            results1.forEach((element1) => {

                var sub_cate_bean = {}
                sub_cate_bean.sub_category_name = element1.hasOwnProperty("sub_category_name") ? element1.sub_category_name : "";
                sub_cate_bean.sub_category_id = element1.hasOwnProperty("sub_category_id") ? element1.sub_category_id : "";

                sub_category_info.push(sub_cate_bean);
            })

            resolve(sub_category_info);
        });
    });
};
