const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');

exports.getCompanyJobs = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        } else {

            get_company_jobs_post_asynch(req, res, connection , '1');
        }
    });
}

exports.getCompanyPost = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        } else {

            get_company_jobs_post_asynch(req, res, connection , '2');
        }
    });
}

const get_company_jobs_post_asynch = async (req, res, connection, type) => {

    var company_id = req.body.user_id
    var recordID = parseInt(req.query.recordID);
    var count = parseInt(req.query.count);

    const promise1 = new Promise(function (resolve, reject) {

        var payload = {};

        var params = [company_id, recordID, count];
        
        let call_procedure = '';
        

        if(type == 1){

            call_procedure = 'CALL get_company_jobs(?,?,?)';
        }
        else if(type == 2){

            call_procedure = 'CALL get_company_post_1(?,?,?)';
        }


            connection.query(call_procedure, params, (error, results) => {

                if (error) {
                    return console.error(error.message);
                }

                payload.connection = connection;

                payload.details = results[0];
                payload.last_rec_id = (recordID + count);
                payload.totalcount = results[1][0].total_count;

                resolve(payload); 

            })
    });

    await promise1.then(function (resultData){

        if(type == 1){

            getDetailsJobs(resultData, resultData.connection).then(function (response) {

                connection.release();
                return res.status(statusCode.genericSuccess.code).send(response);
            });
        }
        else if(type == 2){

            getDetailsPost(resultData, resultData.connection).then(function (response) {

                connection.release();
                return res.status(statusCode.genericSuccess.code).send(response);
            });
        }
        
    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

const getDetailsJobs = (payload, connection) => {

    return new Promise(function (resolve, reject) {

        var response = {};
        response.dataArray = [];
        response.last_rec_id = payload.last_rec_id;
        response.totalcount = payload.totalcount;
        var skill_promis_queue = [];
        var location_promis_queue = [];
        var media_promis_queue = [];
    
    
        payload.details.forEach(element => {
    
            var postData = {};
    
            postData.id = element.hasOwnProperty("id") ? element.id : "";
            postData.title = element.hasOwnProperty("title") ? element.title : "";
            postData.image = element.hasOwnProperty("image") ? element.image : "";
            postData.post_desc = element.hasOwnProperty("job_desc") ? element.job_desc : "";
            postData.date_created = element.hasOwnProperty("created_date_unix") ? element.created_date_unix : "";
            
            //      postData.date_expired = element.hasOwnProperty("date_expired") ? element.date_expired : "";
    
    
            /* if (element.table_name == "company_jobs") */
            {
    
                postData.job_details = {};
    
                
                postData.job_details.active = element.hasOwnProperty("active") ? '' + element.active : 0;
    
                postData.job_details.experience_min = element.hasOwnProperty("experience_min") ? element.experience_min : "";
                postData.job_details.experience_max = element.hasOwnProperty("experience_max") ? element.experience_max : "";
                postData.job_details.salary_min = element.hasOwnProperty("salary_min") ? element.salary_min : "";
                postData.job_details.salary_max = element.hasOwnProperty("salary_max") ? element.salary_max : "";

                postData.job_details.form_id = element.hasOwnProperty("ghf_id") ? element.ghf_id : "";
               // postData.job_details.job_location = element.hasOwnProperty("job_location") ? element.job_location : "";
               // postData.job_details.job_skills = element.hasOwnProperty("job_skills") ? element.job_skills : "";
    
                postData.job_details.remote = element.hasOwnProperty("is_remote") ? '' + element.is_remote : "";
    
    
                postData.job_details.contract = element.hasOwnProperty("contract") ? '' + element.contract : "";
                postData.job_details.job_tags = element.hasOwnProperty("job_tags") ? '' + element.job_tags : "";
                postData.job_details.job_opening_date = element.hasOwnProperty("job_opening_date_1") ? element.job_opening_date_1 : "";
                postData.job_details.job_closing_date = element.hasOwnProperty("job_closing_date_1") ? element.job_closing_date_1 : "";


                skill_promis_queue.push(fetchSkillData(postData.id, connection).then(function (skills_info) {

                    postData.job_details.job_skills = skills_info;
                }));

                location_promis_queue.push(fetchLocationData(postData.id, connection).then(function (job_location_info){

                    postData.job_details.job_location =  job_location_info;

                }));

                media_promis_queue.push(fetchMediaData(postData.id, connection).then(function (data_info) {

                    postData.image = data_info.image;
                    postData.video = data_info.video;
                    postData.attachment = data_info.attachment;
                   
                })); 
            }
    
            response.dataArray.push(postData);
        });

        Promise.all([...skill_promis_queue, ...location_promis_queue, ...media_promis_queue]).then( function (){

            resolve(response);
        });
    })
}


var fetchSkillData = function (post_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query =
            "SELECT name FROM common_skills  where type_id = " + "'" + post_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var skill_info = [];

            result.forEach((element1) => {

                skill_info.push(element1.hasOwnProperty("name") ? element1.name : "");
            })

            resolve(skill_info);
        })
    });
};

var fetchLocationData = function (job_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query =
            "SELECT * FROM company_job_location where job_id = " + "'" + job_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var locationInfo = {};

            result.forEach((element1) => {

                locationInfo.country =  element1.hasOwnProperty("country") ? element1.country : ""
                locationInfo.state =  element1.hasOwnProperty("state") ? element1.state : ""
                locationInfo.city =  element1.hasOwnProperty("city") ? element1.city : ""

            })

            resolve(locationInfo);
        })
    });
};

var fetchMediaData = function (post_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query =  "SELECT * FROM media_post_table where post_id = " + "'" + post_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var dataInfo = {};
            dataInfo.image = [];
            dataInfo.video = [];
            dataInfo.attachment = [];

            result.forEach((element) => {


                if(element.type == 'image'){

                    if(element.hasOwnProperty("media_url")){
                        dataInfo.image.push(element.media_url);
                    }
                }
                else if(element.type == 'video'){

                    if(element.hasOwnProperty("media_url")){
                        dataInfo.video.push(element.media_url);
                    }
                }
                else if(element.type == 'attachment'){

                    if(element.hasOwnProperty("media_url")){
                        dataInfo.attachment.push(element.media_url);
                    }
                }
            })

            resolve(dataInfo);
        })
    });
};


const getDetailsPost = (payload, connection) => {

    return new Promise(function (resolve, reject) {

        var response = {};
        response.dataArray = [];
        response.last_rec_id = payload.last_rec_id;
        response.totalcount = payload.totalcount;
        var media_promis_queue = [];
    
    
        payload.details.forEach(element => {
    
            var postData = {};
    
            postData.id = element.hasOwnProperty("post_id") ? element.post_id : "";
            postData.company_id = element.hasOwnProperty("company_id") ? element.company_id : "";
            postData.title = element.hasOwnProperty("title") ? element.title : "";
            postData.description = element.hasOwnProperty("description") ? element.description : "";
            postData.date_created = element.hasOwnProperty("created_date_unix") ? element.created_date_unix : "";
            
    
            
                media_promis_queue.push(fetchMediaData(postData.id, connection).then(function (data_info) {

                    postData.image = data_info.image;
                    postData.video = data_info.video;
                    postData.attachment = data_info.attachment;
                   
                })); 
    
            response.dataArray.push(postData);
        });

        Promise.all([...media_promis_queue]).then( function (){

            resolve(response);
        });
    })
}



exports.createJob = (req, res) => {
    create_Job(req.body, res);
}

create_Job = (body, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = body;
        var params = [data.company_id, data.title, data.job_desc, data.image, data.experience_min, data.experience_max,
            data.salary_min, data.salary_max, data.job_location, new Date(), new Date(), data.active
        ];


        let get_companyJobs = 'CALL company_create_job(?,?,?,?,?,?,?,?,?,?,?,?)';
        var company_details = {};

        connection.query(get_companyJobs, params, (error, results) => {

            if (error) {

                console.error(error.message);
                return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
            }

            company_details.address = results[0];
            return res.status(statusCode.genericSuccess.code).send(companyModel.address(company_details));

        })


        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });


    exports.editJob = (req, res) => {
        edit_Job(req.body, res);
    }

    edit_Job = (body, res) => {

        db.getConnection(function (err, connection) {

            if (err) {
                callback(true);
                return;
            }


            var data = body;
            var params = [data.company_id, data.title, data.job_id, data.job_desc, data.image, data.experience_min, data.experience_max,
                data.salary_min, data.salary_max, data.job_location, new Date(), new Date(), data.active
            ];


            let call_procedure = 'CALL company_edit_job(?,?,?,?,?,?,?,?,?,?,?,?,?)';
            var company_details = {};

            connection.query(call_procedure, params, (error, results) => {

                if (error) {

                    console.error(error.message);
                    return res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
                }

                company_details.address = results[0];
                return res.status(statusCode.genericSuccess.code).send(companyModel.address(company_details));

            })


            connection.release();

            connection.on('error', function (err) {
                callback(true);
                return;
            });
        });
    }

}