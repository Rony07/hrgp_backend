const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');

exports.getGhfmain = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        get_Ghf_main_asynch(dataObj);
    });
}

const get_Ghf_main_asynch = async (dataObj) => {

    let  connection =  dataObj.connection;
    let  company_id =  dataObj.req.body.user_id;
    let  res        =  dataObj.res;

    const getPromis1 = new Promise(function (resolve, reject) {

        var sql_query = "SELECT main_category_name, company_ghf_main.main_category_id, company_ghf_main.order_num  FROM hrgp.company_ghf_main where company_id = " + "'" + company_id + "'" + " order by company_ghf_main.order_num ";

        connection.query(sql_query, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results;

                resolve(metaObj);
            }
        });
    });


    let waitPromis1 = await getPromis1.then(function (resultData) {

        getGhfMain(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

function getGhfMain(payload, connection) {

    return new Promise(function (resolve, reject) {

        var response = {};
        response.data = [];

        var pqueue = [];

        payload.details.forEach((element, index) => {

            var postData = {};

            postData.main_category_name = element.hasOwnProperty("main_category_name") ? element.main_category_name : "";
            postData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
            postData.order_num = element.hasOwnProperty("order_num") ? element.order_num : "";


            pqueue.push(fetchQueSetData(postData.main_category_id, connection).then(function (sub_category_info) {

                postData.sub_category_info = sub_category_info;
                response.data.push(postData);
            }));
        })

        Promise.all(pqueue).then(function () {

            resolve(response);
        })
    });
}

var fetchQueSetData = function (main_category_id, connection) {

    return new Promise(function (resolve, reject) {

        var sql_query = "SELECT main_category_id, order_num, sub_category_name, sub_category_id  FROM company_ghf_sub where main_category_id = " + "'" + main_category_id + "'" + " order by company_ghf_sub.order_num ";

        connection.query(sql_query, (error, results1) => {
            if (error) {
                return console.error(error.message);
            }

            var sub_category_info = [];

            results1.forEach((element1) => {

                var sub_cate_bean = {}
                sub_cate_bean.sub_category_name = element1.hasOwnProperty("sub_category_name") ? element1.sub_category_name : "";
                sub_cate_bean.sub_category_id = element1.hasOwnProperty("sub_category_id") ? element1.sub_category_id : "";

                sub_category_info.push(sub_cate_bean);
            })

            resolve(sub_category_info);
        });
    });
};

exports.get_AssesmentCategory = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let dataObj = {};

        dataObj.connection = connection;
        dataObj.req = req;
        dataObj.res = res;

        get_AssesmentCategory_asynch(dataObj);
    });
}

const get_AssesmentCategory_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;
    let req = dataObj.req;

    const getPromis1 = new Promise(function (resolve, reject) {

        let params = [company_id];
        let call_procedure = 'CALL get_Assesment_Category(?)'

        connection.query(call_procedure, params,  (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results;
                metaObj.company_id = company_id;


                resolve(metaObj);
            }
        });
    });

    let waitPromis1 = await getPromis1.then(function (resultData) {

        assesment_category(resultData).then(function (response) {

            connection.release();

            return res.status(statusCode.genericSuccess.code).send(mapToObj2(response));

        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

const assesment_category = (resultData) => {

    return new Promise(function (resolve, reject) {

        var resultMap = new Map();
        
        var pqueue = [];
        resultData.details[0].forEach((element, index) => {


            let mainData = {};
            mainData.main_category_name = element.hasOwnProperty("main_category_name") ? element.main_category_name : "";
            mainData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
            mainData.order_num = element.hasOwnProperty("order_num") ? element.order_num : "";
            
            
            let subcatInfo = {};
            subcatInfo.sub_category_name =  element.hasOwnProperty("sub_category_name") ? element.sub_category_name : "";
            subcatInfo.sub_category_id =  element.hasOwnProperty("sub_category_id") ? element.sub_category_id : "";

            
            pqueue.push(fetchQueSetData(mainData.main_category_id, resultData.company_id, resultData.connection).then(function (que_data) {

                mainData.que_data = que_data;
            }));

            if(resultMap.has(mainData.main_category_id)){

                let dataObj = resultMap.get(mainData.main_category_id);
               
                if(subcatInfo.sub_category_id){

                    dataObj.mainData.sub_category_info.push(subcatInfo);
                }
                
                resultMap.set(mainData.main_category_id , dataObj);

            }else{

                let dataObj = {};

                dataObj.mainData = mainData;
                
                dataObj.mainData.sub_category_info =  [];
 
                if(subcatInfo.sub_category_id){
                    
                    dataObj.mainData.sub_category_info.push(subcatInfo);
                }

                resultMap.set(mainData.main_category_id , dataObj);
            }
        })

        Promise.all(pqueue).then(function () {

            setTimeout(() => resolve(resultMap), 200);
        })
    });
}

function mapToObj2(inputMap) {

    let response ={};
    response.dataArray = [];
    let obj = [];

    inputMap.forEach(function (value, key) {
        obj.push(value.mainData);
    });

    response.dataArray = obj;
    return response;
}

var fetchQueSetData = function (main_category_id, company_id, connection) {

    return new Promise(function (resolve, reject) {

        var sql_query = "Select title, id as que_set_id from assesment_main where main_category_id = " + "'" + main_category_id + "'" + " and company_id = " + "'" + company_id + "'";

        connection.query(sql_query, (error, results1) => {
            if (error) {
                return console.error(error.message);
            }


            let que_data = [];

            results1.forEach((element1) => {

                var que_bean = {}
                que_bean.title = element1.hasOwnProperty("title") ? element1.title : "";
                que_bean.que_set_id = element1.hasOwnProperty("que_set_id") ? element1.que_set_id : "";

                que_data.push(que_bean);
            })

            resolve(que_data);
        });
    });
};
