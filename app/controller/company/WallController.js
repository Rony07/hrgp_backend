const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const wallPostModel = require('../../models/company/WallPostModel.js');
const appUtils = require('../../Utils/Utils.js');
const uuidv1 = require('uuid/v1');


exports.getPost = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }


        let sql_query = 'Select user_role_id from users where user_id = ' + req.body.user_id;

        connection.query(sql_query, (error, results) => {

            if (error) {

                appUtils.showLog(statusCode.genericError.code, error, res)

            } else {

                if (results[0].user_role_id == '2') {

                    // Call company wall Post

                    let dataObj = {};
                    dataObj.connection = connection;
                    dataObj.req = req;
                    dataObj.res = res;

                    get_company_post_asynch(dataObj);

                } else if (results[0].user_role_id == '3') {

                    // Call user wall Post

                    let dataObj = {};
                    dataObj.connection = connection;
                    dataObj.req = req;
                    dataObj.res = res;

                    get_user_post_asynch(dataObj);

                }
            }
        })
    });
}


const get_company_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;

    var recordID = parseInt(dataObj.req.query.recordID);
    var count = parseInt(dataObj.req.query.count);

    let params = [company_id, recordID, count];

    const getPromis1 = new Promise(function (resolve, reject) {

        let call_procedure = 'CALL get_company_post(?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results[0];
                metaObj.company_id = company_id;

                resolve(metaObj);
            }
        })
    });

    let waitPromis1 = await getPromis1.then(function (resultData) {


        getWallpostwithComments(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

const get_user_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let company_id = dataObj.req.body.user_id;
    let res = dataObj.res;

    var recordID = parseInt(dataObj.req.query.recordID);
    var count = parseInt(dataObj.req.query.count);

    let params = [company_id, recordID, count];

    const getPromis1 = new Promise(function (resolve, reject) {

        let call_procedure = 'CALL get_company_post_user(?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let metaObj = {};
                metaObj.connection = connection;
                metaObj.details = results[0];
                metaObj.company_id = company_id;

                resolve(metaObj);
            }
        })
    });

    let waitPromis1 = await getPromis1.then(function (resultData) {


        getWallpostwithComments(resultData, resultData.connection).then(function (response) {

            connection.release();
            return res.status(statusCode.genericSuccess.code).send(response);
        });

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}

exports.createPost = (req, res) => {
    create_post(req, res);
}

create_post = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }


        var data = req.body;

        /**
         *  If data.post_type == 1 ( Job Post)
         *  If data.post_type == 2 ( Normal Post)
         */

        var payload = {};

        if (data.post_type == "1") {

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            create_job_post_asynch(dataObj);


        } else if (data.post_type == "2") {

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            create_post_asynch(dataObj);
        }
    });
}

const create_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let data = dataObj.req.body;
    let res = dataObj.res;

    const getPromis1 = new Promise(function (resolve, reject) {

        let params = [uuidv1(), data.user_id, data.title, data.desc, ''/* data.image */, appUtils.formatDateTime(new Date()), data.active];

        let call_procedure = 'CALL company_create_post(?,?,?,?,?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {
                reject(error);
            } else {

                let media_data = getMediaData(params[0], data.image, data.video, data.attachment);

                connection.query("INSERT INTO media_post_table (post_id, type, media_url) VALUES ?", [media_data], (error, results) => {

                    if (error) {

                        reject(error);
                    } else {

                        resolve(params[0]);
                    }
                });
            }
        });
    });


    let waitPromis1 = await getPromis1.then(function (postID) {

            var postData = {};
            postData.message = "Successfully created the post record.";
            postData.last_insert_id = postID;
            return res.status(statusCode.genericSuccess.code).send(postData);
    });

    connection.release();

    connection.on('error', function (err) {
        callback(true);
        return;
    });
}

const create_job_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let data = dataObj.req.body;
    let res = dataObj.res;


    const getPromis1 = new Promise(function (resolve, reject) {

        let params = [uuidv1(), data.user_id, data.title, data.desc, '' /* data.image */ ,
            data.experience_min, data.experience_max, data.salary_min, data.salary_max, '' /* data.job_location */ ,
            data.active, '', '' /* data.job_tags */ , data.remote, data.contract,
            appUtils.formatDateTime(new Date()), data.job_opening_date, data.job_closing_date, data.form_id
        ];

        let call_procedure = 'CALL company_create_job(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                let country = data.job_location.country;
                let state = data.job_location.state;
                let city = data.job_location.city;

                let job_loc_values = [];
                var temp = [data.user_id, params[0], country, state, city];
                job_loc_values.push(temp);


                connection.query("DELETE FROM company_job_location WHERE job_id = ?", params[0], (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }
                    connection.query("INSERT INTO company_job_location (company_id, job_id, country, state, city) VALUES ?", [job_loc_values], (error, results) => {

                        if (error) {

                            reject(error);
                        } else {

                            let media_data = getMediaData(params[0], data.image, data.video, data.attachment);

                            connection.query("INSERT INTO media_post_table (post_id, type, media_url) VALUES ?", [media_data], (error, results) => {

                                if (error) {

                                    reject(error);
                                } else {
                                    // payload.details = results[0];

                                    console.dir(results);

                                    let metaObj = {};
                                    metaObj.connection = connection;
                                    metaObj.job_skills = data.job_skills;
                                    metaObj.postID = params[0];
                                    metaObj.user_id = params[1];

                                    resolve(metaObj);
                                }
                            });
                        }
                    });
                });
            }
        })
        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });

    });


    let waitPromis1 = await getPromis1.then(function (resultData) {

        let values = [];
        var master_values = [];
        let i = 1;
        let job_skills = resultData.job_skills;
        let len = job_skills.length;
        let postID = resultData.postID;
        let user_id = resultData.user_id;

        job_skills.forEach(element => {

            var temp = [user_id, element, 'job_post', postID];
            master_values.push([element]);
            values.push(temp);

            if (i == len) {

                resultData.connection.query("DELETE FROM common_skills WHERE type_id = ?", postID, (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }


                    resultData.connection.query("INSERT INTO common_skills (user_id, name, type, type_id) VALUES ?", [values], (error, results) => {

                        resultData.connection.query("INSERT INTO master_skills (skill) VALUES ?", [master_values], (error, results) => {});

                        if (error) {
                            return console.error(error.message);
                        } else {
                            // payload.details = results[0];
                            var postData = {};
                            postData.message = "Successfully created the Job record.";
                            postData.last_insert_id = postID;
                            return res.status(statusCode.genericSuccess.code).send(postData);
                        }

                    });
                });
            }

            i++;
        })

    }).catch(function (error) {

        appUtils.showLog(statusCode.genericError.code, error, res)
    });
}

const getMediaData = (postID, imageArray, videoArray, attachmentArray) => {

    let media_values = [];

    if(imageArray){

        imageArray.forEach(element => {

            var temp = [postID, 'image', element];
            media_values.push(temp);
        });
    }
    

    if(videoArray){

        videoArray.forEach(element => {

            var temp = [postID, 'video', element];
            media_values.push(temp);
        });
    }
    

    if(attachmentArray){

        attachmentArray.forEach(element => {

            var temp = [postID, 'attachment', element];
            media_values.push(temp);
        });
    }
    

    return media_values;
}


exports.editPost = (req, res) => {
    edit_post(req, res);
}

edit_post = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var params;

        /**
         *  If data.post_type == 1 ( Job Post)
         *  If data.post_type == 2 ( Normal Post)
         */

        var payload = {};

        if (data.post_type == "1") {

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            edit_job_post_asynch(dataObj);


        } else if (data.post_type == "2") {


            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            edit_post_asynch(dataObj);

        }
    });
}

const edit_job_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let data = dataObj.req.body;
    let res = dataObj.res;


    const getPromis1 = new Promise(function (resolve, reject) {


        params = [data.user_id, data.title, data.id, data.desc, ''/* data.image */,
            data.experience_min, data.experience_max, data.salary_min, data.salary_max, '' /* data.job_location */ ,
            data.active, '', '' /* data.job_tags */ , data.remote, data.contract,
            appUtils.formatDateTime(new Date()), data.job_opening_date, data.job_closing_date, data.form_id
        ];

        let call_procedure = 'CALL company_edit_job(?,?,?,?,?,  ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {
                reject(error);
            } else {

                let country = data.job_location.country;
                let state = data.job_location.state;
                let city = data.job_location.city;

                let job_loc_values = [];
                var temp = [data.user_id, data.id, country, state, city];
                job_loc_values.push(temp);


                connection.query("DELETE FROM company_job_location WHERE job_id = ?", data.id, (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }

                    connection.query("INSERT INTO company_job_location (company_id, job_id, country, state, city) VALUES ?", [job_loc_values], (error, results) => {

                        if (error) {

                            reject(error);
                        } else {

                            connection.query("DELETE FROM media_post_table WHERE post_id = ?", data.id, (error, results) => {

                                if (error) {
                                    return console.error(error.message);
                                }

                                let media_data = getMediaData(data.id, data.image, data.video, data.attachment);
                                if(media_data.length >0){

                                    connection.query("INSERT INTO media_post_table (post_id, type, media_url) VALUES ?", [media_data], (error, results) => {

                                        if (error) {
    
                                            reject(error);
                                        } else {
    
                                            console.dir(results);
    
                                            let metaObj = {};
                                            metaObj.connection = connection;
                                            metaObj.job_skills = data.job_skills;
                                            metaObj.postID = data.id;
                                            metaObj.user_id = params[0];
    
                                            resolve(metaObj);
                                        }
                                    });
                                }
                                else{
  
                                    let metaObj = {};
                                    metaObj.connection = connection;
                                    metaObj.job_skills = data.job_skills;
                                    metaObj.postID = data.id;
                                    metaObj.user_id = params[0];

                                    resolve(metaObj);

                                }
                            });
                        }
                    });
                });
            }
        })

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });

    });

    let waitPromis1 = await getPromis1.then(function (resultData) {

        let values = [];
        let master_values = [];
        let i = 1;
        let job_skills = resultData.job_skills;
        let len = job_skills.length;
        let postID = resultData.postID;
        let user_id = resultData.user_id;

        job_skills.forEach(element => {

            var temp = [user_id, element, 'job_post', postID];
            master_values.push([element]);

            values.push(temp);

            if (i == len) {

                resultData.connection.query("DELETE FROM common_skills WHERE type_id = ?", postID, (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    }


                    resultData.connection.query("INSERT INTO common_skills (user_id, name, type, type_id) VALUES ?", [values], (error, results) => {


                        resultData.connection.query("INSERT INTO master_skills (skill) VALUES ?", [master_values], (error, results) => {});

                        if (error) {
                            return console.error(error.message);
                        } else {
                            // payload.details = results[0];
                            var postData = {};
                            postData.message = "Successfully updated the Job record.";
                            postData.last_insert_id = postID;
                            return res.status(statusCode.genericSuccess.code).send(postData);
                        }
                    });
                })
            }

            i++;
        })

    }).catch(function (error) {

        appUtils.showLog(statusCode.genericError.code, error, res)
    });
}

const edit_post_asynch = async (dataObj) => {

    let connection = dataObj.connection;
    let data = dataObj.req.body;
    let res = dataObj.res;

    const getPromis1 = new Promise(function (resolve, reject) {

        let params = [data.company_id, data.title, data.post_id, data.desc, data.image, new Date(), data.active];

        let call_procedure = 'CALL company_edit_post(?,?,?,?,?,?,?)';

        connection.query(call_procedure, params, (error, results) => {

            if (error) {

                reject(error);

            } else {

                connection.query("DELETE FROM media_post_table WHERE post_id = ?", data.post_id, (error, results) => {

                    if (error) {

                        reject(error);
                    }
                    else{

                        let media_data = getMediaData(data.post_id, data.image, data.video, data.attachment);

                        connection.query("INSERT INTO media_post_table (post_id, type, media_url) VALUES ?", [media_data], (error, results) => {
        
                            if (error) {
        
                                reject(error);

                            } else {
        
                                resolve(params[0]);
                            }
                        });
                    }
                });
            }
        });
    });


    let waitPromis1 = await getPromis1.then(function (postID) {

            var postData = {};
            postData.message = "Successfully updated the post record.";
            postData.last_insert_id = postID;
            return res.status(statusCode.genericSuccess.code).send(postData);
    });

    connection.release();

    connection.on('error', function (err) {
        callback(true);
        return;
    });
}



exports.comment = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var payload = {};


        if (data.comment_id) {


            let call_procedure = 'CALL edit_comment(?,?,?,?,?,?)';

            var current_params = [data.comment_id, data.post_id, data.user_id, data.post_comment, appUtils.formatDateTime(new Date()), 1];

            connection.query(call_procedure, current_params, (error, results) => {


                if (error) {
                    appUtils.showLog(statusCode.genericError.code, error, res)
                }


                if (results[0][0].FALSE == undefined) {

                    return res.status(statusCode.genericSuccess.code).send(wallPostModel.success(current_params[0]));
                } else {

                    var postData = {};
                    postData.message = "Post Not found ";

                    return res.status(statusCode.genericError.code).send(postData);
                }

            });

        } else {

            var current_params = [uuidv1(), data.post_id, data.user_id, data.post_comment, appUtils.formatDateTime(new Date()), 1];

            let call_procedure = 'CALL insert_comment(?,?,?,?,?,?)';

            connection.query(call_procedure, current_params, (error, results) => {
                if (error) {
                    appUtils.showLog(statusCode.genericError.code, error, res)
                }

                if (results[0][0].false == undefined) {

                    return res.status(statusCode.genericSuccess.code).send(wallPostModel.success(current_params[0]));
                } else {
                    var postData = {};
                    postData.message = "Post Not found ";
                    return res.status(statusCode.genericError.code).send(postData);
                }

            });
        }

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.edit_comment = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var payload = {};


        var jsonData = JSON.stringify(data.post_comment);

        data.post_comment = jsonData;


        var sql_query = "Update post_comments set  comment_info = " + "'" + data.post_comment + "'" + " where post_id = " + "'" + data.post_id + "'";

        connection.query(sql_query, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(wallPostModel.success(payload));
        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });
}


const getWallpostwithComments = (payload, connection) => {


    return new Promise(function (resolve, reject) {

        var response = {};
        response.data = [];
        var pqueue = [];
        var media_promis_queue = [];
        var skill_promis_queue = [];
        var location_promis_queue = [];

        payload.details.forEach((element) => {

            var postData = {};

            postData.id = element.hasOwnProperty("id") ? element.id : "";
            postData.title = element.hasOwnProperty("title") ? element.title : "";
            //postData.image = element.hasOwnProperty("image") ? element.image : "";
            postData.desc = element.hasOwnProperty("job_desc") ? element.job_desc : "";
            postData.date_created = element.hasOwnProperty("created_date_unix") ? element.created_date_unix : "";

            postData.user_like_status = element.hasOwnProperty("like_status_user") ? element.like_status_user : "0";


            postData.post_num_like = element.hasOwnProperty("post_num_like") ? element.post_num_like : "";
            postData.post_user_name = element.hasOwnProperty("post_user_name") ? element.post_user_name : "";
            postData.post_user_image = element.hasOwnProperty("post_user_image") ? element.post_user_image : "";

            if (element.table_name == "company_jobs") {

                postData.post_type = "1";

                postData.job_details = {};

                postData.job_details.experience_min = element.hasOwnProperty("experience_min") ? element.experience_min : "";
                postData.job_details.experience_max = element.hasOwnProperty("experience_max") ? element.experience_max : "";
                postData.job_details.salary_min = element.hasOwnProperty("salary_min") ? element.salary_min : "";
                postData.job_details.salary_max = element.hasOwnProperty("salary_max") ? element.salary_max : "";
                postData.job_details.form_id = element.hasOwnProperty("ghf_id") ? element.ghf_id : "";
                
                //postData.job_details.job_location = element.hasOwnProperty("job_location") ? element.job_location : "";

                /* if(postData.job_details.job_location.hasOwnProperty("country")){

                    postData.job_details.job_location = JSON.parse(postData.job_details.job_location);
                } */

                postData.job_details.job_skills = element.hasOwnProperty("job_skills") ? element.job_skills : "";

                postData.job_details.remote = element.hasOwnProperty("is_remote") ? element.is_remote : "";
                postData.job_details.contract = element.hasOwnProperty("contract") ? element.contract : "";
                postData.job_details.job_tags = element.hasOwnProperty("job_tags") ? element.job_tags : "";
                postData.job_details.job_opening_date = element.hasOwnProperty("job_opening_date_1") ? element.job_opening_date_1 : "";
                postData.job_details.job_closing_date = element.hasOwnProperty("job_closing_date_1") ? element.job_closing_date_1 : "";


                skill_promis_queue.push(fetchSkillData(postData.id, connection).then(function (skills_info) {

                    postData.job_details.job_skills = skills_info;
                }));

                location_promis_queue.push(fetchLocationData(postData.id, connection).then(function (job_location_info) {

                    postData.job_details.job_location = job_location_info;

                }));


            } else if (element.table_name == "company_post") {

                postData.post_type = "2";
            }

            postData.recordID = '0';
            postData.count = '5';

            pqueue.push(fetchCommentsInfo(postData, connection).then(function (comment_info) {

                postData.comment_info = comment_info;

            }));

            media_promis_queue.push(fetchMediaData(postData.id, connection).then(function (data_info) {

                postData.image = data_info.image;
                postData.video = data_info.video;
                postData.attachment = data_info.attachment;

            }));

            response.data.push(postData);
        })


        Promise.all([...pqueue, ...skill_promis_queue, ...location_promis_queue, ...media_promis_queue]).then(function () {

            resolve(response);
        });

        /* Promise.all(pqueue).then(function () {

            Promise.all(skill_promis_queue).then(function () {

                Promise.all(location_promis_queue).then(function () {
    
                    Promise.all(media_promis_queue).then(function () {
    
                        resolve(response);
                       
                    });
                });
            });
        }); */
    });
}

exports.getPostComments =(req, res) =>{

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }
        else{

            let dataObj = {};
            dataObj.connection = connection;
            dataObj.req = req;
            dataObj.res = res;

            get_post_comments_asynch(dataObj);

        }
    })
}

const get_post_comments_asynch = async (dataObj) => {

    let connection = dataObj.connection;

    var recordID = parseInt(dataObj.req.query.recordID);
    var count = parseInt(dataObj.req.query.count);

    var postData = {};
    postData.id = dataObj.req.query.post_id;
    postData.recordID = recordID;
    postData.count = count;

    
    fetchCommentsInfo(postData, connection).then(function (response) {

        connection.release();
        return dataObj.res.status(statusCode.genericSuccess.code).send(response);

    }).catch(function (error) {

        connection.release();
        appUtils.showLog(statusCode.genericError.code, error, res)

    });
}


var fetchCommentsInfo = function (postData, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query = 'CALL get_post_comments(?,?,?)';
        var params = [postData.id, postData.recordID, postData.count];

        connection.query(sql_query, params, (error, result) => {

            if (error) {

                reject(error);
            }

            var comment_info = [];

            result[0].forEach((element1) => {

                var comments_bean = {}
                comments_bean.comment_id = element1.hasOwnProperty("id") ? element1.id : "";
                comments_bean.post_id = element1.hasOwnProperty("post_id") ? element1.post_id : "";
                comments_bean.user_id = element1.hasOwnProperty("user_id") ? element1.user_id : "";
                comments_bean.comment = element1.hasOwnProperty("comment_info") ? element1.comment_info : "";
                comments_bean.create_date = element1.hasOwnProperty("create_date_unix") ? element1.create_date_unix : "";

                postData.date_modefied = element1.hasOwnProperty("modefied_date") ? element.modefied_date : "";

                var fullName = '';
                var userimage = '';

                if (element1.user_name_a) {

                    fullName = element1.user_name_a;
                } else if (element1.user_name_b) {

                    fullName = element1.user_name_b;
                }


                if (element1.image_a) {

                    userimage = element1.image_a;
                } else if (element1.image_b) {

                    userimage = element1.image_b;
                }


                comments_bean.user_image = userimage;
                comments_bean.user_name = fullName;

                comment_info.push(comments_bean);
            })

            resolve(comment_info);
        })
    });
};

var fetchSkillData = function (post_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query =
            "SELECT name FROM common_skills  where type_id = " + "'" + post_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var skill_info = [];

            result.forEach((element1) => {

                skill_info.push(element1.hasOwnProperty("name") ? element1.name : "");
            })

            resolve(skill_info);
        })
    });
};

var fetchLocationData = function (job_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query =
            "SELECT * FROM company_job_location where job_id = " + "'" + job_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var locationInfo = {};

            result.forEach((element1) => {

                locationInfo.country = element1.hasOwnProperty("country") ? element1.country : ""
                locationInfo.state = element1.hasOwnProperty("state") ? element1.state : ""
                locationInfo.city = element1.hasOwnProperty("city") ? element1.city : ""

            })

            resolve(locationInfo);
        })
    });
};



var fetchMediaData = function (post_id, connection) {

    return new Promise(function (resolve, reject) {

        let sql_query = "SELECT * FROM media_post_table where post_id = " + "'" + post_id + "'";

        connection.query(sql_query, (error, result) => {

            if (error) {
                return console.error(error.message);
            }

            var dataInfo = {};
            dataInfo.image = [];
            dataInfo.video = [];
            dataInfo.attachment = [];

            result.forEach((element) => {


                if (element.type == 'image') {

                    if (element.hasOwnProperty("media_url")) {
                        dataInfo.image.push(element.media_url);
                    }
                } else if (element.type == 'video') {

                    if (element.hasOwnProperty("media_url")) {
                        dataInfo.video.push(element.media_url);
                    }
                } else if (element.type == 'attachment') {

                    if (element.hasOwnProperty("media_url")) {
                        dataInfo.attachment.push(element.media_url);
                    }
                }
            })

            resolve(dataInfo);
        })
    });
};



exports.deleteComment = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var comment_id = req.query.commentID;

        connection.query("Delete from post_comments where id = ?", comment_id, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            var postData = {};
            postData.message = "Successfully Deleted";

            return res.status(statusCode.genericSuccess.code).send(postData);
        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });
}

exports.deletePost = (req, res) => {

    db.getConnection(function (err, connection) {

        if (err) {
            callback(true);
            return;
        }

        var postID = req.query.postID;
        var postType = req.query.postType;

        /**
         *  If post_type == 1 ( Job Post)
         *  If post_type == 2 ( Normal Post)
         */

        var current_params = [postType, req.body.user_id, postID];

        let call_procedure = 'CALL delete_post(?,?,?)';

        connection.query(call_procedure, current_params, (error, results) => {
            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            if (results[0][0].true) {

                var postData = {};
                postData.message = "Successfully Deleted";

                return res.status(statusCode.genericSuccess.code).send(postData);

            } else {

                var postData = {};
                postData.message = "Post Not found ";
                return res.status(statusCode.genericError.code).send(postData);
            }

        });


        connection.release();

        connection.on('error', function (error) {
            return console.error(error.message);
        });
    });

}


exports.add_likes = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var user_id = data.user_id;
        var post_id = data.post_id;
        var like_id = uuidv1();

        var params = [like_id, post_id, user_id, appUtils.formatDateTime(new Date()), appUtils.formatDateTime(new Date())];

        let sql = 'CALL insert_post_likes(?,?,?,?,?)';


        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess.message);

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.remove_likes = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var user_id = data.user_id;
        var post_id = data.post_id;

        var params = [post_id, user_id];

        let sql = 'CALL delete_post_likes(?,?)';


        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess.message);

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.get_post_likes = (req, res) => {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        var data = req.body;
        var post_id = data.post_id;

        var params = [post_id];

        let sql = 'CALL get_post_likes(?)';


        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            return res.status(statusCode.genericSuccess.code).send(results[0]);

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}
