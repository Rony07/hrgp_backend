const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const companyModel = require('../../models/company_model.js');
const appUtils = require('../../Utils/Utils.js');
const appConst = require('../../Utils/Constants.js')


exports.getAllCompanyPublic = (req, res) => {

    get_All_CompanyPublic(req, res);
}

get_All_CompanyPublic = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            
            appUtils.showLog(statusCode.genericError.code, error, res);

        } else {

            let recordID = req.query.recordID ? parseInt(req.query.recordID) : appConst.DEFAULT_RECORD_ID;  
            let count =   req.query.count ? parseInt(req.query.count) : appConst.DEFAULT_RECORD_COUNT ;
    
            let params = [recordID, count];

            let call_procedure = 'CALL get_all_company(?,?)';

            connection.query(call_procedure, params, (error, results) => {

                if (error) {
                    appUtils.showLog(statusCode.genericError.code, error, res);
                }
                else{

                    let payload = {};
                    payload.details = results[0];
                    payload.last_rec_id = (recordID + count);
                    payload.totalcount = results[1][0].total_count;

                    return res.status(statusCode.genericSuccess.code).send(companyModel.getCompanyData(payload));

                }
            });
        }

        connection.release();

    })

}


exports.getprofileCompany = (req, res) => {
    get_company_details(req.body.user_id, res);
}

get_company_details = (company_id, res) => {


    var company_user = {};


    db.getConnection(function (error, connection) {
        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res);
        }

        let sql_query = 'CALL get_company_details(?)';

        connection.query(sql_query, company_id, (error, results) => {
            if (error) {
                  appUtils.showLog(statusCode.genericError.code, error, res);
            }


            company_user.details = results[0][0];

            if (company_user.details) {

                sql_query = "select industry_type.name from industry_type where industry_type.industry_type_id  = " + "'" + company_user.details.industry_type + "'";

                connection.query(sql_query, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    }
                    company_user.industry_type = results[0].name;

                    if (company_user.industry_type) {

                        sql_query = "select * from addresses where addresses.id IN (select company_address.address_id from company_address where company_address.company_id =" + "'" + company_id + "'" + ")";

                        connection.query(sql_query, (error, results) => {
                            if (error) {
                                return console.error(error.message);
                            }
                            company_user.address = results;

                            return res.status(statusCode.genericSuccess.code).send(companyModel.companyInfo(company_user));

                        });

                    } else {

                        company_user.address = [];

                        return res.status(statusCode.genericSuccess.code).send(companyModel.companyInfo(company_user));
                    }
                });
            } else {

                var response = {};
                response.message = "No Data Found";

                return res.status(statusCode.genericSuccess.code).send(response);
            }
        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });

    });
}


exports.editprofile = (req, res) => {
    edit_company_details(req.body, res);
}

edit_company_details = (data, res) => {


    data = data.details;


    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        let sql_query = 'CALL edit_company_profile(?,?,?,?,?,?,?,?,?,?,?,?)';
        var payload = {};

        var params = [data.company_id, data.company_name, data.short_desc, data.image, data.image2, data.email, data.phone_no,
            data.alternate_no, data.about, data.website, data.industry_type, data.company_size
        ];

        connection.query(sql_query, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            payload.details = results[0];
            return res.status(statusCode.genericSuccess.code).send(companyModel.success(payload));

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });

    });
}


exports.addCompanyAddress = (req, res) => {
    add_company_address(req, res);
}

add_company_address = (req, res) => {


    let address = req.body.address;
    let company_id = req.body.user_id;
    let current_params = [address.address1, address.address2, address.city,
        address.state, address.pincode, address.country, address.geo_id
    ];

    let is_main = req.body.is_main;


    let address_sql = 'CALL add_address(?,?,?,?,?,?,?)';

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(address_sql, current_params, (error, results) => {
            if (error) {
                return console.error(error.message);
            } else {

                let current_address_id = results[0][0].insert_id;

                let address_value = [];
                var temp = [current_address_id, is_main, company_id];
                address_value.push(temp);


                connection.query("Insert into company_address(address_id, is_primary, company_id) values ?", [address_value], (error, results) => {

                    if (error) {
                        return console.error(error.message);
                    } else {

                        var postData = {};
                        postData.last_insert_id = current_address_id;
                        postData.message = "Successfully added new address.";
                        return res.status(statusCode.genericSuccess.code).send(postData);
                    }

                });
            }
        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.deleteCompanyAddress = (req, res) => {
    delete_company_address(req, res);
}


delete_company_address = (req, res) => {

    var address_id = req.query.address_id;

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query("DELETE FROM company_address WHERE id = ?", address_id, (error, results) => {
            if (error) {
                return console.error(error.message);
            } else {

                connection.query("DELETE FROM addresses WHERE id = ?", address_id, (error, results) => {
                    if (error) {
                        return console.error(error.message);
                    } else {

                        var postData = {};
                        postData.current_address_id = address_id;
                        postData.message = "Successfully deleted.";
                        return res.status(statusCode.genericSuccess.code).send(postData);
                    }
                });
            }
        });
    });
}


exports.editCompanyAddress = (req, res) => {
    edit_company_address(req, res);
}

edit_company_address = (req, res) => {


    let address = req.body.address;

    let current_params = [address.address_id, address.address1, address.address2, address.city,
        address.state, address.pincode, address.country, address.geo_id
    ];

    let is_main = req.body.is_main;


    let address_sql = 'CALL edit_address(?,?,?,?,?,?,?,?)';

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(address_sql, current_params, (error, results) => {
            if (error) {
                return console.error(error.message);
            } else {

                let current_address_id = results[0][0].insert_id;

                var postData = {};
                postData.last_insert_id = current_address_id;
                postData.message = "Successfully updated address.";
                return res.status(statusCode.genericSuccess.code).send(postData);
            }
        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}

exports.getCompanyAddress = (req, res) => {
    get_company_address(req, res);
}

get_company_address = (req, res) => {

    let address = req.body.address;
    let company_id = req.body.user_id;

    let sql_query = "select * from addresses where addresses.id IN (select address_id from company_address where company_address.company_id =" + "'" + company_id + "'" + ")";

    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }

        connection.query(sql_query, (error, results) => {
            if (error) {
                return console.error(error.message);
            }
            let address = results;

            return res.status(statusCode.genericSuccess.code).send(companyModel.companyAddressInfo(address));

        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}
