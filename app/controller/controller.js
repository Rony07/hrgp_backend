const db = require('../config/db.config.js');
const config = require('../config/config.js');
const statusCode = require('../config/statusCode.js');
const authJwt = require('../router/verifyJwtToken');
const serializeModel = require('../models/signup_model');
const candidateController = require('../controller/candidateController');
const OTP = require('../controller/otp');
const pushController = require('../controller/common/pushController');
var request = require("request");

var bcrypt = require('bcryptjs');


exports.signup = (req, res) => {
	// Save User to Database

	db.getConnection(function (err, connection) {
		if (err) {
			callback(true);
			return;
		}
		var user_id = new Date().getTime();
		var data = req.body;
		var image = data.hasOwnProperty("image") ? data.image : "";
		var type = data.signup_type;
		var params = [user_id, data.email, new Date(), new Date(), data.user_role, 1, bcrypt.hashSync(req.body.password, 8),
			type, data.phone, data.firstname, data.lastname, image, data.browser_token];

		let sql = 'CALL register_user(?,?,?,?,?,?,?,?,?,?,?,?,?)';


		connection.query(sql, params, (error, results) => {
			if (error) {
				return console.error(error.message);
			}

			if (results[0][0].true == undefined) {
				return res.status(statusCode.userAlreadyExists.code).send(statusCode.userAlreadyExists.message);
			} else {
				send_register_mail(req.body.email);
				OTP.send_otp(user_id,type, data.phone, data.email);
				pushController.send_push(data.browser_token, 'Welcome to HRGP. For any queries or guidance contact HRGP Team.', 10000);
				return res.status(statusCode.userRegistered.code).send({
					user_id: user_id,
					email: req.body.email,
					image: image,
					phone: data.phone,
					user_role_id: data.user_role,
					firstname: data.firstname,
					lastname: data.lastname,
					auth: true,
					access_token: authJwt.generateToken(user_id)
				});
			}

		});

		connection.release();

		connection.on('error', function (err) {
			callback(true);
			return;
		});
	});

}


exports.login = (req, res) => {
	db.getConnection(function (error, connection) {
		if (error) {
			return console.error(error.message);
		}

		var params = [req.body.user];

		let sql = 'CALL login_user(?)';


		connection.query(sql, params, (error, results) => {
			if (error) {
				return console.error(error.message);
			}

			if (results[0][0].false == undefined) {
				payload = results[0][0];
				if (req.body.login_type == "google") {
					update_browser_token(payload.user_id, req.body.browser_token);
					payload.auth = true;
					payload.access_token = authJwt.generateToken(payload.user_id);
					return_by_user(res, payload);
				} else if (req.body.login_type == "facebook") {
					update_browser_token(payload.user_id, req.body.browser_token);
					payload.auth = true;
					payload.access_token = authJwt.generateToken(payload.user_id);
					return_by_user(res, payload);
				}
				else {
					var passwordIsValid = bcrypt.compareSync(req.body.password, payload.password);
					if (passwordIsValid) {
						update_browser_token(payload.user_id, req.body.browser_token);
						payload.auth = true;
						payload.access_token = authJwt.generateToken(payload.user_id);
						return_by_user(res, payload);
					} else {
						return res.status(statusCode.invalidUser.code).send(statusCode.invalidUser.message);
					}
				}


			} else {
				return res.status(statusCode.invalidUser.code).send(statusCode.invalidUser.message);
			}

		});

		connection.release();

		connection.on('error', function (err) {
			callback(true);
			return;
		});
	});
}



function return_by_user(res, data) {
	switch (data.user_role_id) {
		case 3:
			return res.status(statusCode.genericSuccess.code).send(serializeModel.login_candidate(data));
			break;
		case 2:
			return res.status(statusCode.genericSuccess.code).send(serializeModel.login_company(data));
		case 1:
			return res.status(statusCode.genericSuccess.code).send(serializeModel.login_admin(data));
			break;
	}
}

function update_browser_token(user_id, token) {
	db.getConnection(function (err, connection) {
		if (err) {
			callback(true);
			return;
		}

		var params = [user_id, token];
		let sql = 'CALL update_browser_token(?,?)';

		connection.query(sql, params, (error, results) => {
			if (error) {
				return console.error(error.message);
			}


			connection.release();

			connection.on('error', function (err) {
				callback(true);
				return;
			});
			return;
		});
	});

}



exports.forgot_password = (req, res) => {

	db.getConnection(function (err, connection) {
		if (err) {
			callback(true);
			return;
		}

		var params = [req.body.email];
		let sql = 'CALL check_user(?)';


		connection.query(sql, params, (error, results) => {
			if (error) {
				return console.error(error.message);
			}



			if (results[0][0].false == undefined) {
				payload = results[0][0];
				send_forgot_mail(req.body.email, payload.user_id);
				return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess.message);

			} else {
				return res.status(statusCode.userNotFound.code).send(statusCode.userNotFound.message);
			}

		});

		connection.release();

		connection.on('error', function (err) {
			callback(true);
			return;
		});
	});

}

exports.change_password = (req, res) => {

	db.getConnection(function (err, connection) {
		if (err) {
			callback(true);
			return;
		}
		var password = bcrypt.hashSync(req.body.password, 8);

		var params = [password, req.body.user_id];
		let sql = 'CALL change_password(?,?)';


		connection.query(sql, params, (error, results) => {
			if (error) {
				return console.error(error.message);
			}


			return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess.message);

		});

		connection.release();

		connection.on('error', function (err) {
			callback(true);
			return;
		});
	});

}

function send_register_mail(email) {

	var options = {
		method: 'POST',
		url: 'http://18.191.220.132/mail/send.php',
		headers:
		{
			'Postman-Token': '76166407-18a3-4bf0-bb2a-a8d9f81611e2',
			'cache-control': 'no-cache',
			'Content-Type': 'application/json'
		},
		body:
		{
			email: email,
			subject: 'Welcome to HRGP',
			name: 'name',
			message: '<h2 style=\'text-align: center;\'>Welcome User to HRGP</h2><p>You can now go to your dashboard and explore the platform.</p><p>Stay tuned more coming soon.</p><p>Thank You</p><p>Team HRGP</p><p><span style=\'text-decoration: underline; color: #0000ff;\'>www.hrgp.com</span></p><p>&nbsp;</p>'
		},
		json: true
	};

	request(options, function (error, response, body) {
		if (error) throw new Error(error);

		console.log(body);
	});
}

function send_forgot_mail(mail, user_id) {
	var options = {
		method: 'POST',
		url: 'http://18.191.220.132/mail/send.php',
		headers:
		{
			'Postman-Token': '76166407-18a3-4bf0-bb2a-a8d9f81611e2',
			'cache-control': 'no-cache',
			'Content-Type': 'application/json'
		},
		body:
		{
			email: mail,
			subject: 'Forgot Password',
			name: 'name',
			message: '<html><h2 style="text-align: center;"><strong>Email to change password</strong>&nbsp;</h2><p>Hello, please click on the link below to verify the changing of YOUR password. You will be asked to change the password and re-enter the new password.</p><p>&nbsp;</p><p><span style="text-decoration: underline; color: #0000ff;"<em>http://34.73.12.38/#/forgotpassword/' + user_id + '</em></span></p><p>Thank you from HRGP team!</p></html>'
		},
		json: true
	};

	request(options, function (error, response, body) {
		if (error) throw new Error(error);

		console.log(body);
	});
}





