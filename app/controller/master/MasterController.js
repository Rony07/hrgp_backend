const db = require('../../config/db.config.js');
const statusCode = require('../../config/statusCode.js');
const appUtils = require('../../Utils/Utils.js');


exports.getMasterSkills = (req, res) => {
    get_master_skills(req, res);
}

get_master_skills = (req, res) => {


    db.getConnection(function (error, connection){

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        let sql_query = 'select distinct skill from master_skills';
        let resultArray = [];

        connection.query(sql_query, (error, results)=>{

            if(error){
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            results.forEach(element => {
                resultArray.push(element.skill);
            });

            return res.status(statusCode.genericSuccess.code).send(resultArray);

         });
       
        connection.release();

        connection.on('error', function (err) {
            appUtils.showLog(statusCode.genericError.code, err, res)
        });
    });
}
exports.getCompanyPositionAll = (req, res) => {
    get_CompanyPositionAll(req, res);
}

get_CompanyPositionAll = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        var sql_query = "Select distinct position_name from master_company_positions";
        let resultArray = [];

        connection.query(sql_query, (error, results)=>{

            if(error){
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            results.forEach(element => {
                resultArray.push(element.position_name);
            });

            return res.status(statusCode.genericSuccess.code).send(resultArray);

         });

        connection.release();
        connection.on('error', function (err) {

            appUtils.showLog(statusCode.genericError.code, err, res)
        });
    });
}

exports.getMasterCertificate = (req, res) => {
    get_MasterCertificate(req, res);
}

get_MasterCertificate = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        var sql_query = "SELECT distinct certificate_name FROM master_certificates";
        let resultArray = [];

        connection.query(sql_query, (error, results)=>{

            if(error){
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            results.forEach(element => {
                resultArray.push(element.certificate_name);
            });

            return res.status(statusCode.genericSuccess.code).send(resultArray);

         });

        connection.release();
        connection.on('error', function (err) {

            appUtils.showLog(statusCode.genericError.code, err, res)
        });
    });
}


exports.getMasterCompanies = (req, res) => {
    get_MasterCompanies(req, res);
}

get_MasterCompanies = (req, res) => {

    db.getConnection(function (error, connection) {

        if (error) {
            appUtils.showLog(statusCode.genericError.code, error, res)
        }

        var sql_query = "Select distinct company_name from master_company";
        let resultArray = [];

        connection.query(sql_query, (error, results)=>{

            if(error){
                appUtils.showLog(statusCode.genericError.code, error, res)
            }

            results.forEach(element => {
                resultArray.push(element.company_name);
            });

            return res.status(statusCode.genericSuccess.code).send(resultArray);

         });

        connection.release();
        connection.on('error', function (err) {

            appUtils.showLog(statusCode.genericError.code, err, res)
        });
    });
}
exports.getAllRecords = (req, res) => {
    get_AllRecords(req, res);
}

const get_AllRecords  = async(req, res) => {

    var type_array = req.body.type;
    var allPromis_que = [];

    db.getConnection(function (error, connection) {

        if (error) {

            appUtils.showLog(statusCode.genericError.code, error, res)
        }
        else{

            let response = {};
            response.data = [];

            type_array.forEach(element => {
 
                allPromis_que.push(fetchData(element, connection, res).then(function (data_info) {
 
                    response.data.push(data_info);
                }));
            });

            Promise.all(allPromis_que).then(function () {

                connection.release();
               return res.status(statusCode.genericSuccess.code).send(response);
            });

        }
    });
}

var fetchData = function (post_type, connection, res) {

    return new Promise(function (resolve, reject) {


        let sql_query = '';

        if(post_type == 'skills'){

            sql_query = 'select distinct skill from master_skills';
        }
        else if(post_type == 'companyPositions'){

            sql_query = 'Select distinct position_name from master_company_positions';
        }
        else if(post_type == 'certificates'){

            sql_query = 'SELECT distinct certificate_name FROM master_certificates';
        }
        else if(post_type == 'company'){

            sql_query = 'Select distinct company_name from master_company';
        }
        

        connection.query(sql_query, (error, result) => {

            if (error) {
                appUtils.showLog(statusCode.genericError.code, error, res)
            }


            var data_info = {};
            data_info.masterArray = [];

            result.forEach((element1) => {

                if(post_type == 'skills'){

                    data_info.masterArray.push(element1.hasOwnProperty("skill") ? element1.skill : "");
                    data_info.type = post_type;
                }
                else if(post_type == 'companyPositions'){
        
                    data_info.masterArray.push(element1.hasOwnProperty("position_name") ? element1.position_name : "");
                    data_info.type = post_type;
                }
                else if(post_type == 'certificates'){
        
                    data_info.masterArray.push(element1.hasOwnProperty("certificate_name") ? element1.certificate_name : "");
                    data_info.type = post_type;
                }
                else if(post_type == 'company'){
        
                    data_info.masterArray.push(element1.hasOwnProperty("company_name") ? element1.company_name : "");
                    data_info.type = post_type;
                }
            });

            resolve(data_info);
        })
    });
};
