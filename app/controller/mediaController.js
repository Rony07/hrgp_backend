const db = require('../config/db.config.js');
const config = require('../config/config.js');
const statusCode = require('../config/statusCode.js');

const {Storage} = require('@google-cloud/storage');
const projectId = 'cityretails-884b6';

var fs = require('fs');
var express =   require("express");
var multer  =   require('multer');
var path = require('path');


// Local Upload

var storage1 =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now()+path.extname(file.originalname));   
  }
});

var uploadtest = multer({ storage : storage1}).single('files');

exports.upload = (req,res) => {


  uploadtest(req,res,function(err) {
    if(err) {
      return res.end("Error uploading file.");
    }
    uploadFile("./uploads/"+req.file.filename);
    return res.status(statusCode.genericSuccess.code).send({filename: req.file.filename});
  });
     
}





async function uploadFile(filename) {

    const storage = new Storage({
        projectId: projectId,
      });

      var myBucket = storage.bucket('hrgp_storage');
    await myBucket.upload(filename, {
      gzip: true,
      metadata: {
         cacheControl: 'public, max-age=31536000',
      },
    });
    fs.unlinkSync(filename);
  
    // console.log(`${filename} uploaded to ${myBucket}.`);
  }
