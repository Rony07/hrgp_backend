const db = require('../config/db.config.js');
const config = require('../config/config.js');
const statusCode = require('../config/statusCode.js');
const smsController = require('../controller/common/smsController');
const pushController = require('../controller/common/pushController');
const emailController = require('../controller/common/emailController');

exports.send_otp = (user_id, type, phone, email) => {
    if (type != "normal") {
        // phone_otp = generate_otp();
        // insert_otp(user_id, "phone", phone_otp);
        // smsController.send_message("+91" + phone, "Hello user your verification code is " + phone_otp);
        email_otp = generate_otp();
        phone_otp = generate_otp();
        insert_otp(user_id, "phone", phone_otp);
        insert_otp(user_id, "email", email_otp);
        smsController.send_message("+91" + phone, "Hello user your verification code is " + phone_otp);
        emailController.send_message(email, "HRGP Verification Code", '<html><h2 style="text-align: center;"><strong>HRGP</strong></h2><p style="text-align: center;"><strong>Verification Code</strong></p><p style="text-align: left;">&nbsp;</p><p style="text-align: left;">Hello user your email verification code is : ' + email_otp + '</p><p style="text-align: left;">Thank You,</p><p style="text-align: left;">Team HRGP</p></html>');
    } else {
        email_otp = generate_otp();
        phone_otp = generate_otp();
        insert_otp(user_id, "phone", phone_otp);
        insert_otp(user_id, "email", email_otp);
        smsController.send_message("+91" + phone, "Hello user your verification code is " + phone_otp);
        emailController.send_message(email, "HRGP Verification Code", '<html><h2 style="text-align: center;"><strong>HRGP</strong></h2><p style="text-align: center;"><strong>Verification Code</strong></p><p style="text-align: left;">&nbsp;</p><p style="text-align: left;">Hello user your email verification code is : ' + email_otp + '</p><p style="text-align: left;">Thank You,</p><p style="text-align: left;">Team HRGP</p></html>');
    }
    return;
}



exports.verify_otp = (req, res) => {
    var data = req.body;


    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var params = [data.user_id, data.otp, data.type];

        let sql = 'CALL verify_otp(?,?,?)';
        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

            if (results[0][0].true == undefined) {
                return res.status(statusCode.otpError.code).send(statusCode.otpError.message);
            } else {
                return res.status(statusCode.genericSuccess.code).send(statusCode.genericSuccess.message);
            }
        });

        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}



function generate_otp() {
    return Math.floor(Math.random() * 899999 + 100000);
}

function insert_otp(user_id, type, code) {
    db.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            return;
        }
        var params = [user_id, code, type];

        let sql = 'CALL add_otp(?,?,?)';


        connection.query(sql, params, (error, results) => {
            if (error) {
                return console.error(error.message);
            }

        });
        connection.release();

        connection.on('error', function (err) {
            callback(true);
            return;
        });
    });
}