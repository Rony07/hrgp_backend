exports.assesment_main = (payload) => {

    var response = {};
    response.data = [];

    

    payload.details.forEach(element => {

        var postData = {};


        if(element.hasOwnProperty("id")){
        
            postData.assesment_id = element.id ;
        }

        postData.title = element.hasOwnProperty("title") ? element.title : "";
        postData.desc = element.hasOwnProperty("ass_desc") ? element.ass_desc : "";
        postData.image = element.hasOwnProperty("image") ? element.image : "";
        postData.isTimer = element.hasOwnProperty("is_timer") ? element.is_timer : "";
        postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";
        postData.active = element.hasOwnProperty("active") ? element.active : "";
        postData.job_skills = element.hasOwnProperty("job_skills") ? element.job_skills : "";
        postData.visible_question = element.hasOwnProperty("visible_question") ? element.visible_question : "";
        postData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
        postData.sub_category_id = element.hasOwnProperty("sub_category_id") ? element.sub_category_id : "";

        

        response.data.push(postData);
    })

    return response;
}


exports.assesment_data = (payload) => {

    var response = {};
    response.data = [];

    

    payload.details.forEach(element => {

        var postData = {};

        postData.que_id = element.hasOwnProperty("id") ? element.id : "";
        postData.ass_test_id = element.hasOwnProperty("assesment_test_id") ? element.assesment_test_id : "";
        postData.que_title = element.hasOwnProperty("que_title") ? element.que_title : "";
        postData.que_media_type = element.hasOwnProperty("que_media_type") ? element.que_media_type : "";
        postData.ans_media_type = element.hasOwnProperty("ans_media_type") ? element.ans_media_type : "";
        postData.ans_option = element.hasOwnProperty("ans_option") ? element.ans_option : "";
        postData.ans_data =  element.hasOwnProperty("ans_data") ? element.ans_data : "";

        if(postData.ans_data && IsJsonString(postData.ans_data)){
            postData.ans_data = JSON.parse(postData.ans_data)
        }

        postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";
        postData.active = element.hasOwnProperty("active") ? element.active : "";
        

        response.data.push(postData);
    })

    return response;
}


function IsJsonString(str) {
    try {
      var json = JSON.parse(str);
      return (typeof json === 'object');
    } catch (e) {
      return false;
    }
  }

exports.success = (type) => {

    var postData = {};

    postData.message = "Successfully Done";


    return postData;
}
