



candidate = (payload) => {

    var response = {};
    response.user_id = payload.details.hasOwnProperty("user_id") ? payload.details.user_id:0;
    response.email = payload.details.hasOwnProperty("email") ? payload.details.email:"";
    response.date_created = payload.details.hasOwnProperty("date_created") ? payload.details.date_created:"0";
    response.user_role_id = payload.details.hasOwnProperty("user_role_id") ? payload.details.user_role_id:"";
    
    response.user_details = {};
    response.user_details.firstname = payload.details.hasOwnProperty("first_name") ? payload.details.first_name:"";
    response.user_details.lastname = payload.details.hasOwnProperty("last_name") ? payload.details.last_name:"";
    response.user_details.image = payload.details.hasOwnProperty("image") ? payload.details.image:"";
    response.user_details.image2 = payload.details.hasOwnProperty("image2") ? payload.details.image2:"";
    response.user_details.phone_no = payload.details.hasOwnProperty("phone_no") ? payload.details.phone_no:"";
    response.user_details.alternate_no = payload.details.hasOwnProperty("alternate_no") ? payload.details.alternate_no:"";
    response.user_details.about_me = payload.details.hasOwnProperty("about_me") ? payload.details.about_me:"";
   
    response.current_address = {};
    response.current_address.address1 = payload.details.hasOwnProperty("address_1") ? payload.details.address_1:"";
    response.current_address.address2 = payload.details.hasOwnProperty("address_2") ? payload.details.address_2:"";
    response.current_address.city = payload.details.hasOwnProperty("city") ? payload.details.city:"";
    response.current_address.state = payload.details.hasOwnProperty("state") ? payload.details.state:"";
    response.current_address.pincode = payload.details.hasOwnProperty("pincode") ? payload.details.pincode:"";
    response.current_address.country = payload.details.hasOwnProperty("country") ? payload.details.country:"";
    response.current_address.geo_id = payload.details.hasOwnProperty("geo_id") ? payload.details.geo_id:"";
    
    response.permanent_address = {};
    response.permanent_address.address1 = payload.details.hasOwnProperty("permanent_address_1") ? payload.details.permanent_address_1:"";
    response.permanent_address.address2 = payload.details.hasOwnProperty("permanent_address_2") ? payload.details.permanent_address_2:"";
    response.permanent_address.city = payload.details.hasOwnProperty("permanent_city") ? payload.details.permanent_city:"";
    response.permanent_address.state = payload.details.hasOwnProperty("permanent_state") ? payload.details.permanent_state:"";
    response.permanent_address.pincode = payload.details.hasOwnProperty("permanent_pincode") ? payload.details.permanent_pincode:"";
    response.permanent_address.country = payload.details.hasOwnProperty("permanent_country") ? payload.details.permanent_country:"";
    response.permanent_address.geo_id = payload.details.hasOwnProperty("permanent_geo_id") ? payload.details.permanent_geo_id:"";
 
    response.skills = [];
    response.skills = payload.skills.length > 0 ? payload.skills : [];
    response.qualifications = [];
    response.qualifications = payload.qualifications.length>0 ? payload.qualifications : [];
    response.expierence = [];

    response.expierence = payload.expierence.length>0 ? payload.expierence : [];
    response.certification = [];
    response.certification = payload.certification.length>0 ? payload.certification : [];

    response.preferred_location = [];
    response.preferred_location = payload.preferred_location.length>0 ? payload.preferred_location : [];
    return response;
 }
 
 
 
 const serializeModel = {};
 serializeModel.login_candidate = login_candidate;
 
 module.exports = serializeModel;
 
 
 
 const candidateModel = {};
 candidateModel.candidate = candidate;
 
 module.exports = candidateModel;