exports.getQuestionData = (queArrayData) => {

    var response = {};
    response.que_data = [];

    if(queArrayData){

        queArrayData.forEach(element =>{

            var que_obj = {};
             
             que_obj.que_id = element.hasOwnProperty("id") ? element.id:""; 
             que_obj.que_title = element.hasOwnProperty("que_title") ? element.que_title:""; 
             que_obj.ans_data = element.hasOwnProperty("ans_data") ? element.ans_data:"";

             response.que_data.push(que_obj)
        });
    }
    return response;
 }
