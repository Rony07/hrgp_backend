exports.getAllEmployees = (payload) => {

    var response = {};
    response.dataArray = [];
    response.last_rec_id = payload.last_rec_id;
    response.totalcount = payload.totalcount;


    payload.details.forEach(element => {

        var postData = {};

        postData.emp_id = element.hasOwnProperty("emp_id") ? element.emp_id : "";
        postData.employee_company_id = element.hasOwnProperty("employee_company_id") ? element.employee_company_id : "";
        postData.company_id = element.hasOwnProperty("company_id") ? element.company_id : "";
        postData.first_name = element.hasOwnProperty("first_name") ? element.first_name : "";
        postData.last_name = element.hasOwnProperty("last_name") ? element.last_name : "";

        postData.email = element.hasOwnProperty("email") ? element.email : "";
        postData.phone_num = element.hasOwnProperty("phone_num") ? element.phone_num : "";
        postData.date_of_birth = element.hasOwnProperty("date_of_birth") ? element.date_of_birth : "";
        postData.experience = element.hasOwnProperty("experience") ? element.experience : "";
        postData.designation = element.hasOwnProperty("designation") ? element.designation : "";
       
        postData.location = element.hasOwnProperty("location") ? element.location : "";
        postData.emp_image = element.hasOwnProperty("emp_image") ? element.emp_image : "";
        postData.emp_description = element.hasOwnProperty("emp_description") ? element.emp_description : "";
        postData.job_start_date = element.hasOwnProperty("job_start_date") ? element.job_start_date : "";
        postData.job_end_date = element.hasOwnProperty("job_end_date") ? element.job_end_date : "";
        postData.job_type = element.hasOwnProperty("job_type") ? element.job_type : "";
        postData.active = element.hasOwnProperty("active") ? element.active : "";


        response.dataArray.push(postData);
    })

    return response;
}


