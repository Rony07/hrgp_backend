const statusCode = require('../../config/statusCode.js');

exports.getform_builder_main = (payload) => {

    var response = {};
    response.data = [];



    payload.details.forEach(element => {

        var postData = {};


        postData.form_id = element.hasOwnProperty("form_id") ? element.form_id : "";
        postData.form_text_id = element.hasOwnProperty("form_text_id") ? element.form_text_id : "";
        postData.form_title = element.hasOwnProperty("form_title") ? element.form_title : "";
        postData.form_data = element.hasOwnProperty("form_data") ? JSON.parse(element.form_data) : "";
        postData.company_position = element.hasOwnProperty("company_position") ? element.company_position : "";

        postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";
        postData.date_modefied = element.hasOwnProperty("date_modefied") ? element.date_modefied : "";

        postData.active = element.hasOwnProperty("active") ? element.active : "";
        response.data.push(postData);
    })

    return response;
}




function mapToObj2(inputMap) {
    let obj = {};

    inputMap.forEach(function (value, key) {
        obj[key] = value
    });

    return obj;
}


exports.getCompanyPos = (payload) => {

    var response = {};
    response.data = [];

    payload.details.forEach(element => {

        var postData = {};



        postData.position_name = element.hasOwnProperty("position_name") ? element.position_name : "";
        postData.position_id = element.hasOwnProperty("position_id") ? element.position_id : "";

        response.data.push(postData);
    })

    return response;
}


exports.getGhfMain = (payload) => {

    var response = {};
    response.data = [];

    payload.details.forEach(element => {

        var postData = {};

        postData.main_category_name = element.hasOwnProperty("main_category_name") ? element.main_category_name : "";
        postData.main_category_id = element.hasOwnProperty("main_category_id") ? element.main_category_id : "";
        postData.order_num = element.hasOwnProperty("order_num") ? element.order_num : "";
        postData.sub_category_name = element.hasOwnProperty("sub_category_name") ? element.sub_category_name : "";

        response.data.push(postData);
    })

    return response;
}




exports.success = (type) => {

    var postData = {};

    postData.message = "Successfully updated";


    return postData;
}