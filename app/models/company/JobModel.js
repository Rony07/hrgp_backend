

getJobs = (payload) => {

    var response = {};
    response.dataArray = [];
    response.last_rec_id = payload.last_rec_id;
    response.totalcount = payload.totalcount;


    payload.details.forEach(element => {

        var postData = {};

        postData.title = element.hasOwnProperty("title") ? element.title : "";
        postData.image = element.hasOwnProperty("image") ? element.image : "";
        postData.post_desc = element.hasOwnProperty("job_desc") ? element.job_desc : "";
        postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";
      //      postData.date_expired = element.hasOwnProperty("date_expired") ? element.date_expired : "";


        /* if (element.table_name == "company_jobs") */ {

            postData.job_details = {};

            postData.job_details.post_id = element.hasOwnProperty("id") ? element.id : "";
            postData.job_details.active = element.hasOwnProperty("active") ? ''+element.active  : 0;
            
            postData.job_details.experience_min = element.hasOwnProperty("experience_min") ? element.experience_min : "";
            postData.job_details.experience_max = element.hasOwnProperty("experience_max") ? element.experience_max : "";
            postData.job_details.salary_min = element.hasOwnProperty("salary_min") ? element.salary_min : "";
            postData.job_details.salary_max = element.hasOwnProperty("salary_max") ? element.salary_max : "";

            postData.job_details.job_location = element.hasOwnProperty("job_location") ? element.job_location : "";
            postData.job_details.job_skills = element.hasOwnProperty("job_skills") ? element.job_skills : "";

            postData.job_details.remote = element.hasOwnProperty("is_remote") ? ''+element.is_remote : "";


            postData.job_details.contract = element.hasOwnProperty("contract") ? ''+element.contract : "";
            postData.job_details.job_tags = element.hasOwnProperty("job_tags") ? ''+element.job_tags : "";
            postData.job_details.job_opening_date = element.hasOwnProperty("job_opening_date") ? element.job_opening_date : "";
            postData.job_details.job_closing_date = element.hasOwnProperty("job_closing_date") ? element.job_closing_date : "";
        }


        response.dataArray.push(postData);
    })

    return response;
}



const jobModel = {};
jobModel.getJobs = getJobs;

module.exports = jobModel;