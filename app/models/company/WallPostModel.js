

getWallpost = (payload) => {

    var response = {};
    response.data = [];


    for (var key in payload) {

        if (payload.hasOwnProperty(key)) {

            var objectInfo = payload[key];

            objectInfo.forEach(element => {

                var postData = {};

                postData.id     = element.hasOwnProperty("id") ? element.id : "";
                postData.title  = element.hasOwnProperty("title") ? element.title : "";
                postData.image  = element.hasOwnProperty("image") ? element.image : "";
                postData.desc   = element.hasOwnProperty("job_desc") ? element.job_desc : "";
                postData.date_created = element.hasOwnProperty("date_created") ? element.date_created : "";


                if (element.table_name == "company_jobs") {
                    
                    postData.type = "2";

                    postData.job_details = {};

                    postData.job_details.experience_min = element.hasOwnProperty("experience_min") ? element.experience_min : "";
                    postData.job_details.experience_max = element.hasOwnProperty("experience_max") ? element.experience_max : "";
                    postData.job_details.salary_min = element.hasOwnProperty("salary_min") ? element.salary_min : "";
                    postData.job_details.salary_max = element.hasOwnProperty("salary_max") ? element.salary_max : "";

                    postData.job_details.job_location = element.hasOwnProperty("job_location") ? element.job_location : "";
                    postData.job_details.job_skills = element.hasOwnProperty("job_skills") ? element.job_skills : "";
                }
                else if (element.table_name == "company_post") {
                    
                    postData.type = "1";
                }


                response.data.push(postData);
            })
        }
    }

    return response;
}

success = (type) =>{

    var postData = {};

    postData.message = "Successfully created : ";
    postData.lastInsertId = type;


    return postData;
}

getSearchResult = (payload) => {


    var response = {};
    response.data = [];


    for (var key in payload) {

        if (payload.hasOwnProperty(key)) {

            var objectInfo = payload[key];

            objectInfo.forEach(element => {

               // console.log(element);

                response.data.push(element);
            })
        }
    }

    return response;
}


getCommentModel = (payload) => {

    var response = {};
    response.data = [];

    
    payload.details.forEach(element => {

        var postData = {};

        postData.comment_id = element.hasOwnProperty("id") ? element.id : "";
        postData.company_id = element.hasOwnProperty("company_id") ? element.company_id : "";
        postData.post_id = element.hasOwnProperty("post_id") ? element.post_id : "";
        postData.user_id = element.hasOwnProperty("user_id") ? element.user_id : "";
        postData.comment_info = element.hasOwnProperty("comment_info") ? element.comment_info : "";

        postData.date_created = element.hasOwnProperty("create_date") ? element.create_date : "";
        postData.active = element.hasOwnProperty("is_active") ? element.is_active : "";
        

        response.data.push(postData);
    })

    return response;
}



const serializeModel = {};
serializeModel.getWallpost = getWallpost;
serializeModel.success = success;
serializeModel.getSearchResult = getSearchResult;
serializeModel.getCommentModel = getCommentModel;



module.exports = serializeModel;