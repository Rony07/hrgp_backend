



companyInfo = (payload) => {

    var response = {};
    /* response.email = payload.details.hasOwnProperty("email") ? payload.details.email:"";
    response.date_created = payload.details.hasOwnProperty("date_created") ? payload.details.date_created:"0";
    response.user_role_id = payload.details.hasOwnProperty("user_role_id") ? payload.details.user_role_id:""; */
    
    response.details = {};
    response.details.company_name = payload.details.hasOwnProperty("company_name") ? payload.details.company_name:"";
    response.details.short_desc = payload.details.hasOwnProperty("short_desc") ? payload.details.short_desc:"";
    response.details.image = payload.details.hasOwnProperty("image") ? payload.details.image:"";
    response.details.image2 = payload.details.hasOwnProperty("image2") ? payload.details.image2:"";
    response.details.email = payload.details.hasOwnProperty("email") ? payload.details.email:"";
    
    response.details.phone_no = payload.details.hasOwnProperty("phone_no") ? payload.details.phone_no:"";
    response.details.alternate_no = payload.details.hasOwnProperty("alternate_no") ? payload.details.alternate_no:"";
    response.details.about = payload.details.hasOwnProperty("about") ? payload.details.about:"";

    response.details.website = payload.details.hasOwnProperty("website") ? payload.details.website:"";
    response.details.industry_type = payload.hasOwnProperty("industry_type") ? payload.industry_type:"";
    response.details.company_size = payload.details.hasOwnProperty("company_size") ? payload.details.company_size:"";

    var address = payload.address;

    response.address_data = [];

    if(address){

        address.forEach(element =>{

            var current_address = {};

             current_address.address_id = element.hasOwnProperty("id") ? element.id:"";   
             current_address.address1 = element.hasOwnProperty("address_1") ? element.address_1:"";
             current_address.address2 = element.hasOwnProperty("address_2") ? element.address_2:"";
             current_address.city = element.hasOwnProperty("city") ? element.city:"";
             current_address.state = element.hasOwnProperty("state") ? element.state:"";
             current_address.pincode = element.hasOwnProperty("pincode") ? element.pincode:"";
             current_address.country = element.hasOwnProperty("country") ? element.country:"";
             current_address.geo_id = element.hasOwnProperty("geo_id") ? element.geo_id:""; 


             response.address_data.push(current_address)
        });
    
       
    }
    
   
   
    return response;
 }

 companyAddressInfo = (address) => {

    var response = {};
    var addressArray = address;

    response.address_data = [];

    if(addressArray){

        addressArray.forEach(element =>{

            var current_address = {};
             
            current_address.address_id = element.hasOwnProperty("id") ? element.id:""; 
             current_address.address1 = element.hasOwnProperty("address_1") ? element.address_1:"";
             current_address.address2 = element.hasOwnProperty("address_2") ? element.address_2:"";
             current_address.city = element.hasOwnProperty("city") ? element.city:"";
             current_address.state = element.hasOwnProperty("state") ? element.state:"";
             current_address.pincode = element.hasOwnProperty("pincode") ? element.pincode:"";
             current_address.country = element.hasOwnProperty("country") ? element.country:"";
             current_address.geo_id = element.hasOwnProperty("geo_id") ? element.geo_id:""; 

             response.address_data.push(current_address)
        });
    }
    return response;
 }


 success = (type) =>{

    var postData = {};

    postData.message = "Successfully Done";


    return postData;
}

getCompanyData = (payload) => {

    var response = {};
    response.dataArray = [];
    response.last_rec_id = payload.last_rec_id;
    response.totalcount = payload.totalcount;


    payload.details.forEach(element => {

        var postData = {};

        postData.company_id = element.hasOwnProperty("company_id") ? element.company_id : "";
        postData.company_name = element.hasOwnProperty("company_name") ? element.company_name : "";
        postData.company_size = element.hasOwnProperty("company_size") ? element.company_size : "";
        postData.followers = element.hasOwnProperty("followers") ? element.followers : "";

        postData.image = element.hasOwnProperty("image") ? element.image : "";
        postData.short_desc = element.hasOwnProperty("short_desc") ? element.short_desc : "";
        postData.website = element.hasOwnProperty("website") ? element.website : "";
        

      
        response.dataArray.push(postData);
    })

    return response;
}

 
 
 const companyModel = {};
 companyModel.companyInfo = companyInfo;
 companyModel.success = success;
 companyModel.companyAddressInfo = companyAddressInfo;
 companyModel.getCompanyData = getCompanyData;
 
 module.exports = companyModel;