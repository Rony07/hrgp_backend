

login_candidate = (payload) => {

   var response = {};
   response.user_id = payload.hasOwnProperty("user_id") ? payload.user_id:0;
   response.email = payload.hasOwnProperty("email") ? payload.email:"";
   response.date_created = payload.hasOwnProperty("date_created") ? payload.date_created:"0";
   response.user_role_id = payload.hasOwnProperty("user_role_id") ? payload.user_role_id:"";
   response.auth = payload.hasOwnProperty("auth") ? payload.auth:"";
   response.access_token = payload.hasOwnProperty("access_token") ? payload.access_token:"";
   response.user_details = {};
   response.user_details.firstname = payload.hasOwnProperty("first_name") ? payload.first_name:"";
   response.user_details.lastname = payload.hasOwnProperty("last_name") ? payload.last_name:"";
   response.user_details.image = payload.hasOwnProperty("image") ? payload.image:"";
   response.user_details.phone_no = payload.hasOwnProperty("phone_no") ? payload.phone_no:"";
   response.user_details.alternate_no = payload.hasOwnProperty("alternate_no") ? payload.alternate_no:"";
   response.user_details.about_me = payload.hasOwnProperty("about_me") ? payload.about_me:"";
   response.current_address = {};
   response.current_address.address1 = payload.hasOwnProperty("address_1") ? payload.address_1:"";
   response.current_address.address2 = payload.hasOwnProperty("address_2") ? payload.address_2:"";
   response.current_address.city = payload.hasOwnProperty("city") ? payload.city:"";
   response.current_address.state = payload.hasOwnProperty("state") ? payload.state:"";
   response.current_address.pincode = payload.hasOwnProperty("pincode") ? payload.pincode:"";
   response.current_address.country = payload.hasOwnProperty("country") ? payload.country:"";
   response.current_address.geo_id = payload.hasOwnProperty("geo_id") ? payload.geo_id:"";
   response.permanent_address = {};
   response.permanent_address.address1 = payload.hasOwnProperty("permanent_address_1") ? payload.permanent_address_1:"";
   response.permanent_address.address2 = payload.hasOwnProperty("permanent_address_2") ? payload.permanent_address_2:"";
   response.permanent_address.city = payload.hasOwnProperty("permanent_city") ? payload.permanent_city:"";
   response.permanent_address.state = payload.hasOwnProperty("permanent_state") ? payload.permanent_state:"";
   response.permanent_address.pincode = payload.hasOwnProperty("permanent_pincode") ? payload.permanent_pincode:"";
   response.permanent_address.country = payload.hasOwnProperty("permanent_country") ? payload.permanent_country:"";
   response.permanent_address.geo_id = payload.hasOwnProperty("permanent_geo_id") ? payload.permanent_geo_id:"";

   return response;
}


login_company = (payload) => {

    var response = {};
    response.user_id = payload.hasOwnProperty("user_id") ? payload.user_id:0;
    response.email = payload.hasOwnProperty("email") ? payload.email:"";
    response.date_created = payload.hasOwnProperty("date_created") ? payload.date_created:"0";
    response.user_role_id = payload.hasOwnProperty("user_role_id") ? payload.user_role_id:"";
    response.auth = payload.hasOwnProperty("auth") ? payload.auth:"";
    response.access_token = payload.hasOwnProperty("access_token") ? payload.access_token:"";
    response.user_details = {};
    response.user_details.company_name = payload.hasOwnProperty("company_name") ? payload.company_name:"";
    response.user_details.short_desc = payload.hasOwnProperty("short_desc") ? payload.short_desc:"";
    response.user_details.image = payload.hasOwnProperty("company_image") ? payload.company_image:"";
    response.user_details.phone_no = payload.hasOwnProperty("phone_no") ? payload.phone_no:"";
    response.user_details.alternate_no = payload.hasOwnProperty("alternate_no") ? payload.alternate_no:"";
    response.user_details.about = payload.hasOwnProperty("company_about") ? payload.company_about:"";
   
 
    return response;
 }


 login_admin = (payload) => {

   var response = {};
   response.user_id = payload.hasOwnProperty("user_id") ? payload.user_id:0;
   response.email = payload.hasOwnProperty("email") ? payload.email:"";
   response.date_created = payload.hasOwnProperty("date_created") ? payload.date_created:"0";
   response.user_role_id = payload.hasOwnProperty("user_role_id") ? payload.user_role_id:"";
   response.auth = payload.hasOwnProperty("auth") ? payload.auth:"";
   response.access_token = payload.hasOwnProperty("access_token") ? payload.access_token:"";
   
   return response;
}



const serializeModel = {};
serializeModel.login_candidate = login_candidate;
serializeModel.login_company = login_company;
serializeModel.login_admin = login_admin;


module.exports = serializeModel;