
const authJwt = require('./verifyJwtToken');

module.exports = function (app) {
	//Sign up controllers
	const controller = require('../controller/controller.js');
	const mediaController = require('../controller/mediaController');
	const signUpVerify = require('../verify/signup.js');
	const edtCompanyVerify = require('../verify/Company.js');

	// Candidate Controllers
	const candidateController = require('../controller/candidateController.js');
	const candidateVerify = require('../verify/candidate.js');

	// Company Controllers
	const companyController = require('../controller/company/compnayController.js');
	const jobsController = require('../controller/company/JobsController.js');
	const assesmentController = require('../controller/company/AssesmentController.js');
	const wallController = require('../controller/company/WallController.js');
	const formBuilderController = require('../controller/company/FormBuilderController.js')
	const guidedHFMController = require('../controller/company/GuidedHFMain.js')

	const jobSearchController = require('../controller/JobSearchController.js');

	// Admin Controllers
	const templateController = require('../controller/admin/templateController');
	const adminVerify = require('../verify/admin');
	const adminUserController = require('../controller/admin/userController');

	//const testController = require('../controller/company/TestController.js')

	const followController = require('../controller/company/FollowUnfollowController.js');


    const employeeController = require('../controller/company/EmployeeController.js');

	// OTP
	const otp = require('../controller/otp');

	// Master Controller
	const masterController = require('../controller/master/MasterController.js');

	const applyJobController = require('../controller/company/ApplyJobController.js');

	// Upload media
	app.post('/api/uploadmedia', mediaController.upload);

	// Sign in / Sign up 
	app.post('/api/auth/signup', [signUpVerify.signup_verify], controller.signup);
	app.post('/api/auth/login', signUpVerify.login_verify, controller.login);
	app.post('/api/forgotpassword', signUpVerify.forgot_password_verify, controller.forgot_password);
	app.post('/api/changepassword', signUpVerify.change_password_verify, controller.change_password);
	app.post('/api/verifyotp', signUpVerify.verify_otp, otp.verify_otp)
	

	// Candidate edit profile
	app.post('/api/edit/candidate', [authJwt.verifyToken, candidateVerify.edit_verify], candidateController.edit);
	app.get('/api/profile/candidate', authJwt.verifyToken, candidateController.getprofile);


	// Job Search
	app.post('/api/jobsearch', authJwt.verifyToken, jobSearchController.jobsearch);

	// Company 
	app.get('/api/getcompanyprofile', authJwt.verifyToken, companyController.getprofileCompany);
	app.post('/api/editcompanyprofile', [authJwt.verifyToken, edtCompanyVerify.editProfile_verify], companyController.editprofile);
	app.post('/api/addCompanyAddress', authJwt.verifyToken, companyController.addCompanyAddress);
	app.get('/api/getCompanyAddress', authJwt.verifyToken, companyController.getCompanyAddress);
	app.post('/api/editCompanyAddress', authJwt.verifyToken, companyController.editCompanyAddress);
	app.delete('/api/deleteCompanyAddress', authJwt.verifyToken, companyController.deleteCompanyAddress);
	app.get('/api/getAllCompanyInfo', authJwt.verifyToken, companyController.getAllCompanyPublic);	

	// Company Employee
	app.post('/api/addEmployee', authJwt.verifyToken, employeeController.addEmployee);
	app.get('/api/getEmployees', authJwt.verifyToken, employeeController.getEmployees);

	app.get('/api/getCompanyJobs', authJwt.verifyToken, jobsController.getCompanyJobs);
	app.get('/api/getCompanyPost', authJwt.verifyToken, jobsController.getCompanyPost);
	//app.post('/api/createJob', authJwt.verifyToken, jobsController.createJob);
	
	app.post('/api/createAssesmentMain', authJwt.verifyToken, assesmentController.createAssesment);
	app.post('/api/editAssesmentMain', authJwt.verifyToken, assesmentController.editAssesment);
	app.get('/api/getAssesmentMain', authJwt.verifyToken, assesmentController.getAssesment);
	app.delete('/api/deleteAssesmentMain', authJwt.verifyToken, assesmentController.deleteAssesmentMain);

	app.post('/api/createAssesmentQue', authJwt.verifyToken, assesmentController.createAssesmentQue);
	app.post('/api/editAssesmentQue', authJwt.verifyToken, assesmentController.editAssesmentQue);
	app.post('/api/getAssesmentdata', authJwt.verifyToken, assesmentController.getAssesmentdata);
	app.post('/api/editQuestionOrder', authJwt.verifyToken, assesmentController.editQuestionOrder);
	app.delete('/api/deleteQuestion', authJwt.verifyToken, assesmentController.deleteQuestion);


	// WallPost
	app.get('/api/getWallPost', authJwt.verifyToken, wallController.getPost);
	app.post('/api/createPost', authJwt.verifyToken, wallController.createPost);
	app.post('/api/editPost', authJwt.verifyToken, wallController.editPost);
	app.post('/api/deletePost', authJwt.verifyToken, wallController.deletePost);
	

	// Post Comments
	app.get('/api/getcomment', authJwt.verifyToken, wallController.getPostComments);
	app.post('/api/commentPost', authJwt.verifyToken, wallController.comment);

	// Post Likes
	app.post('/api/addlike', authJwt.verifyToken, wallController.add_likes);
	app.post('/api/removelike', authJwt.verifyToken, wallController.remove_likes);
	app.post('/api/getpostlikes', authJwt.verifyToken, wallController.get_post_likes);

	// Admin 
	app.get('/admin/pushevents', templateController.pushEvents);
	app.post('/admin/addtemplate', adminVerify.template_verify, templateController.addTemplate)
	app.get('/admin/pushtemplates',templateController.getTemplates);
	app.post('/admin/changetemplateactive', adminVerify.active_template_verify, templateController.changeTemplateActive);
	app.post('/admin/edittemplate', adminVerify.edit_template_verify, templateController.editTemplate);
	app.post('/admin/deletetemplate', adminVerify.delete_template_verify ,templateController.deleteTemplate);
	app.post('/admin/getusers', adminVerify.user_verify, adminUserController.getUsers);

	app.delete('/api/deleteComment', authJwt.verifyToken, wallController.deleteComment);
	
	// Form Builder
	app.post('/api/createFormBuilder', authJwt.verifyToken, formBuilderController.createFormBuilder);
	app.post('/api/editFormBuilder', authJwt.verifyToken, formBuilderController.editFormBuilder);
	app.get('/api/getFormBuilder', authJwt.verifyToken, formBuilderController.getFormBuilder);
	app.get('/api/getAllForm', authJwt.verifyToken, formBuilderController.getAllForm);
	app.get('/api/getFormBuilderID', authJwt.verifyToken, formBuilderController.getFormBuilderID);
	app.delete('/api/delete_ghf', authJwt.verifyToken, formBuilderController.delete_ghf);
	//app.get('/api/testghf_main', authJwt.verifyToken, testController.getGhfmain);

	app.get('/api/getCompanyPositions', authJwt.verifyToken, formBuilderController.getCompanyPosition);
	app.get('/api/getGhfmain', authJwt.verifyToken, guidedHFMController.getGhfmain);
	app.get('/api/getQuestionersCategory', authJwt.verifyToken, formBuilderController.get_QuestionersCategory);


	//JobSeeker Company Follow
	app.post('/api/followcompany', authJwt.verifyToken, followController.followCompany);
	app.get('/api/getfollowList', authJwt.verifyToken, followController.getfollowList);
	app.post('/api/unfollowCompany', authJwt.verifyToken, followController.unfollowCompany);
	

    // Master Controller
	app.get('/api/getAllSkills', authJwt.verifyToken,masterController.getMasterSkills);  
	app.get('/api/getAllPosition', authJwt.verifyToken,masterController.getCompanyPositionAll);  
	app.get('/api/getAllCertificates', authJwt.verifyToken,masterController.getMasterCertificate);  
	app.get('/api/getAllCompany', authJwt.verifyToken,masterController.getMasterCompanies);  
	
	app.post('/api/getAllRecords', authJwt.verifyToken,masterController.getAllRecords);
















	// TEST ROUTES
	const assesmentFunctions = require('../controller/assessment/assessmentFunctions');
	app.get('/test/initialInsert', assesmentFunctions.initialInsert);
}