const jwt = require('jsonwebtoken');
const config = require('../config/config.js');
const db = require('../config/db.config.js');
const statusCode = require('../config/statusCode');


verifyToken = (req, res, next) => {
	let token = req.headers['x-access-token'];
  
	if (!token){
		return res.status(statusCode.noTokenProvided.code).send(statusCode.noTokenProvided.message);
	}

	jwt.verify(token, config.secret, (err, decoded) => {
		if (err){
			return res.status(statusCode.tokenVerficationFailed.code).send(statusCode.tokenVerficationFailed.message);
		}
		req.body.user_id = decoded.id;
		next();
	});
}

generateToken = (user_id) => {
	var token = jwt.sign({ id: user_id}, config.secret, {
		expiresIn: 86400 // expires in 24 hours
	});
	return token;
}

const authJwt = {};
authJwt.verifyToken = verifyToken;
authJwt.generateToken = generateToken;

module.exports = authJwt;