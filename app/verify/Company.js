const statusCode = require('../config/statusCode.js');

exports.editProfile_verify = (req, res, next) => {

    var data = req.body.details;

    /* var params = [data.company_id, data.company_name, data.short_desc, data.image, data.image2, data.email, data.phone_no,
        data.alternate_no, data.about, data.website, data.industry_type, data.company_size
    ]; */

    var typeInt = parseInt(data.industry_type);

    if(!data.company_id  || ! Number.isInteger(typeInt) )
    {
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}
