const statusCode = require('../config/statusCode.js');

template_verify = (req, res, next) => {

    var data = req.body;
    if(!data.hasOwnProperty("event_id") || !data.hasOwnProperty("email_subject") || !data.hasOwnProperty("email_content")
        || !data.hasOwnProperty("sms_template") || !data.hasOwnProperty("push_template")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

edit_template_verify = (req, res, next) => {

    var data = req.body;
    if(!data.hasOwnProperty("event_id") || !data.hasOwnProperty("email_subject") || !data.hasOwnProperty("email_content")
        || !data.hasOwnProperty("sms_template") || !data.hasOwnProperty("push_template") || !data.hasOwnProperty("id")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

active_template_verify = (req, res, next) => {
    var data = req.body;
    if(!data.hasOwnProperty("event_id") || !data.hasOwnProperty("id") || !data.hasOwnProperty("active")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

delete_template_verify = (req, res, next) => {
    var data = req.body;
    if(!data.hasOwnProperty("id")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

user_verify = (req, res, next) => {
    var data = req.body;
    if(!data.hasOwnProperty("user_id") || !data.hasOwnProperty("filter") || !data.hasOwnProperty('result_count')){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

const adminVerify = {};
adminVerify.template_verify = template_verify;
adminVerify.edit_template_verify = edit_template_verify;
adminVerify.active_template_verify = active_template_verify;
adminVerify.delete_template_verify = delete_template_verify;
adminVerify.user_verify = user_verify;

module.exports = adminVerify;