const statusCode = require('../config/statusCode.js');

edit_verify = (req, res, next) => {

    var data = req.body;
    if(!data.hasOwnProperty("user_id") || !data.hasOwnProperty("firstname") || !data.hasOwnProperty("lastname")
        || !data.hasOwnProperty("image") || !data.hasOwnProperty("address")
        ||  !data.hasOwnProperty("qualifications") || !data.hasOwnProperty("job_expierence") || 
        !data.hasOwnProperty("certifications") || !data.hasOwnProperty("skills")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

const candidateVerify = {};
candidateVerify.edit_verify = edit_verify;

module.exports = candidateVerify;