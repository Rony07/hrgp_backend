const statusCode = require('../config/statusCode.js');

signup_verify = (req, res, next) => {

    var data = req.body;
    if(!data.hasOwnProperty("email") || !data.hasOwnProperty("firstname") || !data.hasOwnProperty("lastname")
        || !data.hasOwnProperty("password") || !data.hasOwnProperty("phone") || !data.hasOwnProperty("user_role")
        || !data.hasOwnProperty("signup_type") || !data.hasOwnProperty("browser_token")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

login_verify = (req, res, next)=> {

    var data = req.body;
    if(!data.hasOwnProperty("user") || !data.hasOwnProperty("password") || !data.hasOwnProperty("login_type")
    || !data.hasOwnProperty("browser_token")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}


company_signup_verify = (req, res, next) => {

    var data = req.body;
    if(!data.hasOwnProperty("email") || !data.hasOwnProperty("company_name") 
        || !data.hasOwnProperty("password") || !data.hasOwnProperty("phone") || !data.hasOwnProperty("user_role")
        || !data.hasOwnProperty("signup_type") || !data.hasOwnProperty("browser_token")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

company_login_verify = (req, res, next)=> {

    var data = req.body;
    if(!data.hasOwnProperty("user") || !data.hasOwnProperty("password") || !data.hasOwnProperty("login_type")
    || !data.hasOwnProperty("browser_token")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

forgot_password_verify = (req,res,next) => {
    var data = req.body;
    if(!data.hasOwnProperty("email")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

change_password_verify = (req,res,next) => {
    var data = req.body;
    if(!data.hasOwnProperty("user_id") || !data.hasOwnProperty("password")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}

verify_otp = (req,res,next) => {
    var data = req.body;
    if(!data.hasOwnProperty("user_id") || !data.hasOwnProperty("otp") || !data.hasOwnProperty("type")){
        res.status(statusCode.paramsNotFound.code).send(statusCode.paramsNotFound.message);
        return;
    }
    next();
}


const signUpVerify = {};
signUpVerify.signup_verify = signup_verify;
signUpVerify.login_verify = login_verify;
signUpVerify.company_signup_verify = company_signup_verify;
signUpVerify.company_login_verify = company_login_verify;
signUpVerify.forgot_password_verify = forgot_password_verify;
signUpVerify.change_password_verify = change_password_verify;
signUpVerify.verify_otp = verify_otp;



module.exports = signUpVerify;