var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
const fileUpload = require('express-fileupload');
app.use(bodyParser.json(), cors())

require('./app/router/router.js')(app);

const db = require('./app/config/db.config.js');

// Create a Server
var server = app.listen(8080, function () {

    var host = server.address().address
    var port = server.address().port

    console.log("App listening at http://%s:%s", host, port)
})

