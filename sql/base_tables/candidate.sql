-- MySQL dump 10.13  Distrib 8.0.14, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: hrgp
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 \ent     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `addresses` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `address_1` varchar(200) DEFAULT NULL,
  `address_2` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `geo_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (348,'	asad','asd','asd','asfasd','asf','sd','asd'),(349,'	asad222','asd222','asd222','asfasd222','asf222','sd','asd'),(350,'	asad222','asd222','asd222','asfasd222','asf222','sd','asd');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_certifications`
--

DROP TABLE IF EXISTS `candidate_certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidate_certifications` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_date` varchar(255) DEFAULT NULL,
  `to_date` varchar(255) DEFAULT NULL,
  `number` varchar(600) DEFAULT NULL,
  `provider` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `candidate_certifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_certifications`
--

LOCK TABLES `candidate_certifications` WRITE;
/*!40000 ALTER TABLE `candidate_certifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_certifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_details`
--

DROP TABLE IF EXISTS `candidate_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidate_details` (
  `user_id` varchar(60) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `current_address` int(60) DEFAULT NULL,
  `permanent_address` int(60) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `alternate_no` varchar(255) DEFAULT NULL,
  `about_me` varchar(5000) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `current_address` (`current_address`),
  KEY `permanent_address` (`permanent_address`),
  CONSTRAINT `candidate_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `candidate_details_ibfk_2` FOREIGN KEY (`current_address`) REFERENCES `addresses` (`id`),
  CONSTRAINT `candidate_details_ibfk_3` FOREIGN KEY (`permanent_address`) REFERENCES `addresses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_details`
--

LOCK TABLES `candidate_details` WRITE;
/*!40000 ALTER TABLE `candidate_details` DISABLE KEYS */;
INSERT INTO `candidate_details` VALUES ('1549459676514','Ak','test','test.png',NULL,NULL,'9660118822',NULL,NULL);
/*!40000 ALTER TABLE `candidate_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_job_expierence`
--

DROP TABLE IF EXISTS `candidate_job_expierence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidate_job_expierence` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `from_date` varchar(255) DEFAULT NULL,
  `to_date` varchar(255) DEFAULT NULL,
  `present` varchar(255) DEFAULT '0',
  `designation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `candidate_job_expierence_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_job_expierence`
--

LOCK TABLES `candidate_job_expierence` WRITE;
/*!40000 ALTER TABLE `candidate_job_expierence` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_job_expierence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_qualifications`
--

DROP TABLE IF EXISTS `candidate_qualifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidate_qualifications` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_date` varchar(255) DEFAULT NULL,
  `to_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `candidate_qualifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_qualifications`
--

LOCK TABLES `candidate_qualifications` WRITE;
/*!40000 ALTER TABLE `candidate_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_qualifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_skills`
--

DROP TABLE IF EXISTS `candidate_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidate_skills` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `candidate_skills_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_skills`
--

LOCK TABLES `candidate_skills` WRITE;
/*!40000 ALTER TABLE `candidate_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `candidate_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_address`
--

DROP TABLE IF EXISTS `company_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company_address` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `address_id` varchar(60) NOT NULL,
  `is_primary` tinyint(4) DEFAULT '0',
  `company_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_address`
--

LOCK TABLES `company_address` WRITE;
/*!40000 ALTER TABLE `company_address` DISABLE KEYS */;
INSERT INTO `company_address` VALUES (11,'348',0,'1549610293140'),(12,'349',0,'1549610293149'),(13,'350',1,'1549610293149');
/*!40000 ALTER TABLE `company_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_data`
--

DROP TABLE IF EXISTS `company_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company_data` (
  `company_id` varchar(20) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `short_desc` varchar(100) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `image2` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `about` varchar(1000) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `industry_type` int(60) DEFAULT NULL,
  `company_size` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `alternate_no` varchar(20) DEFAULT NULL,
  KEY `company_id` (`company_id`),
  KEY `industry_type` (`industry_type`),
  KEY `company_data_ibfk_2_idx` (`company_id`),
  CONSTRAINT `company_data_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `company_data_ibfk_3` FOREIGN KEY (`industry_type`) REFERENCES `industry_type` (`industry_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_data`
--

LOCK TABLES `company_data` WRITE;
/*!40000 ALTER TABLE `company_data` DISABLE KEYS */;
INSERT INTO `company_data` VALUES ('123','Test Company','Hello World','image.png',NULL,NULL,NULL,NULL,NULL,NULL,'8946516',NULL),('147','XXTEST','Hello World','image.png',NULL,NULL,NULL,NULL,NULL,NULL,'9988776655',NULL),('1549610293149','My Test Company','Hello World','image.png','image.png',NULL,'Hello World New World','www.google.com',NULL,'45','79879879',NULL);
/*!40000 ALTER TABLE `company_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_job_location`
--

DROP TABLE IF EXISTS `company_job_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company_job_location` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `address_id` varchar(60) NOT NULL,
  `company_id` varchar(60) NOT NULL,
  `job_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_job_location`
--

LOCK TABLES `company_job_location` WRITE;
/*!40000 ALTER TABLE `company_job_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_job_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_jobs`
--

DROP TABLE IF EXISTS `company_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company_jobs` (
  `id` int(60) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `job_desc` varchar(200) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `experience_min` varchar(2) DEFAULT NULL,
  `experience_max` varchar(2) DEFAULT NULL,
  `salary_min` varchar(10) DEFAULT NULL,
  `salary_max` varchar(10) DEFAULT NULL,
  `job_location` varchar(500) DEFAULT NULL,
  `date_created` varchar(60) DEFAULT NULL,
  `date_expired` varchar(60) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `job_skills` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_f_id_idx` (`company_id`),
  CONSTRAINT `company_f_id` FOREIGN KEY (`company_id`) REFERENCES `company_data` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_jobs`
--

LOCK TABLES `company_jobs` WRITE;
/*!40000 ALTER TABLE `company_jobs` DISABLE KEYS */;
INSERT INTO `company_jobs` VALUES (1,'1549610293149','Java-Developer','Java Developer With 7 Years of Experience ','image.png','4','7','4','10','{',NULL,NULL,'1',NULL),(2,'1549610293149','XXX','XXX','XXX','X','X','X','X','XXX','XXX','XXX','0',NULL),(3,'1549610293149','React JS + Majento Develoepr','Candiadate Require with React + Majento Developer','image.png','2','12','5','50','locationA, Location, B','147258369','17777788','1',NULL),(4,'1549610293149','Java Developer ','Greeting Aman Solution required a Java Develope','img.png','5','10','12','16','Jaipur','2019-02-12 15:17:33.768','2019-02-12 15:17:33.768','1',NULL),(5,'1549610293149','Java Developer ','Greeting Aman Solution required a Java Develope','img.png','5','10','12','16','Jaipur','2019-02-12 15:25:25.428','2019-02-12 15:25:25.428','1',NULL),(6,'1549610293149','Java Developer ','Greeting Aman Solution required a Java Develope','img.png','5','10','12','16','Jaipur','2019-02-12 15:33:54.610','2019-02-12 15:33:54.610','1',NULL);
/*!40000 ALTER TABLE `company_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `industry_type`
--

DROP TABLE IF EXISTS `industry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `industry_type` (
  `industry_type_id` int(60) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`industry_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `industry_type`
--

LOCK TABLES `industry_type` WRITE;
/*!40000 ALTER TABLE `industry_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `industry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_roles` (
  `user_role_id` int(60) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (2,'Company'),(3,'JobSeeker');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_created` varchar(255) DEFAULT NULL,
  `last_logged_in` varchar(255) DEFAULT NULL,
  `user_role_id` int(60) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `password` varchar(600) DEFAULT NULL,
  `signup_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `user_role_id` (`user_role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('123','company@gmail.com','07022019','07022019',2,'1','1','1'),('147','test@company2.com','000','000',2,'1','147258369','google'),('1549459676514','ak@test.com','2019-02-06 18:57:56.514','2019-02-06 18:57:56.514',3,'1','$2a$08$XBAsSbaRyNlvrXM7Lyy6y.hbVO2aNTWLHDXEDIFJnZYjk1nOTrDTK','normal'),('1549610293149','ak@company.com','2019-02-08 12:48:13.149','2019-02-08 12:48:13.149',2,'1','$2a$08$o97LFS7WEaEzwgBWHysv1OmfanuJ0DQAnLhYbLJzN6HdKB5vfP9Ny','normal');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-12 15:53:57
