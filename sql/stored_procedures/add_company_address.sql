CREATE DEFINER=`root`@`localhost` PROCEDURE `add_company_address`(
IN c_company_id varchar(255),
IN u_address_1 varchar(500),
IN u_address_2 varchar(500),
IN u_city varchar(255),
IN u_state varchar(255),
IN u_pincode varchar(255),
IN u_country varchar(255),
IN u_geo_id varchar(1000)
)
BEGIN
    
    
    INSERT INTO addresses (address_1, address_2, city, state, pincode, country, geo_id)
	VALUES (u_address_1, u_address_2, u_city, u_state, u_pincode, u_country, u_geo_id);
    
	SET @transactID =	(SELECT LAST_INSERT_ID());
    
    Insert Into company_address(address_id, company_id) VALUES (@transactID, c_company_id);
END