CREATE DEFINER=`root`@`localhost` PROCEDURE `candidate_details`(
IN u_user_id varchar(255)
)
BEGIN
       SELECT a.user_id, a.email, a.date_created, a.user_role_id, a.signup_type,
       b.first_name, b.last_name, b.image,
       b.phone_no, b.alternate_no , b.about_me, c.address_1, c.address_2, c.city, c.state,
       c.pincode, c.country, c.geo_id, d.address_1 as permanent_address_1, d.address_2 as permanent_address_2, 
       d.city as permanent_city, d.state as permanent_state,d.pincode as permanent_pincode, d.country as permanent_country,
       d.geo_id as permanent_geo_id
       FROM users a
       LEFT JOIN candidate_details b ON a.user_id = b.user_id
       LEFT JOIN addresses c ON b.current_address = c.id
       LEFT JOIN addresses d ON b.permanent_address = d.id
       WHERE a.user_id = u_user_id;

END