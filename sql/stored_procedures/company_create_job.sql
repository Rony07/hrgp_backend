CREATE DEFINER=`root`@`localhost` PROCEDURE `company_create_job`(

IN c_company_id varchar(20),
IN c_title varchar(50),
IN c_desc varchar(200),
IN c_image varchar(50),
IN c_experience_min varchar(50),
IN c_experience_max varchar(50),
IN c_salary_min varchar(50),
IN c_salary_max varchar(50),
IN c_job_location varchar(50),
IN c_created varchar(60),
IN c_expired varchar(60),
IN c_active varchar(1)
)
BEGIN
   
	INSERT INTO company_jobs(company_id, 	title, 	job_desc, 	image, 	experience_min,		experience_max, 	salary_min, 	salary_max, 	job_location, date_created, date_expired, active)
	VALUES 					(c_company_id, c_title, c_desc, 	c_image, c_experience_min, 	c_experience_max, 	c_salary_min, 	c_salary_max,	c_job_location, c_created, 	c_expired, c_active);
		
END