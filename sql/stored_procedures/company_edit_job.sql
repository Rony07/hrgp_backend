CREATE DEFINER=`root`@`localhost` PROCEDURE `company_edit_job`(

IN c_id varchar(20),
IN c_company_id varchar(20),
IN c_title varchar(50),
IN c_desc varchar(200),
IN c_image varchar(50),
IN c_experience_min varchar(50),
IN c_experience_max varchar(50),
IN c_salary_min varchar(50),
IN c_salary_max varchar(50),
IN c_job_location varchar(50),
IN c_created varchar(60),
IN c_expired varchar(60),
IN c_active varchar(1)
)
BEGIN
   
	UPDATE  company_jobs SET 
       title = c_title,
       job_desc = c_desc,
       image = c_image,
       experience_min = c_experience_min,
       experience_max = c_experience_max,
       salary_min = c_salary_min,
       salary_max =c_salary_max ,
       job_location = c_job_location,
       date_created = c_created,
       date_expired = c_expired,
       active = c_active
	
      Where  id = c_id &&  company_id = c_company_id;
		
END