CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_candidate`(
IN u_user_id varchar(255),
IN u_firstname varchar(255),
IN u_lastname varchar(255),
IN u_image varchar(300),
IN u_current_address int(30),
IN u_permanent_address int(30),
IN u_about_me varchar(1000)
)
BEGIN
    UPDATE candidate_details SET first_name = u_firstname, last_name = u_lastname, image = u_image,
    current_address = u_current_address, permanent_address = u_permanent_address, about_me = u_about_me
    WHERE user_id = u_user_id;
    SELECT "true";
END