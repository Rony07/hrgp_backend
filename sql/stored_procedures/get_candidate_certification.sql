CREATE DEFINER=`root`@`localhost` PROCEDURE `get_candidate_certification`(
IN u_user_id varchar(255)
)
BEGIN

    SELECT * FROM candidate_certifications WHERE user_id = u_user_id;

END