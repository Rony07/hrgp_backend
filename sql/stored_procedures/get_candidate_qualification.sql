CREATE DEFINER=`root`@`localhost` PROCEDURE `get_candidate_qualification`(
IN u_user_id varchar(255)
)
BEGIN

    SELECT * FROM candidate_qualifications WHERE user_id = u_user_id;

END