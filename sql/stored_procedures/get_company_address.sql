CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_address`(
IN company_id varchar(255)
)
BEGIN
SELECT  a.address_1, a.address_2, a.city, a.state, a.pincode,a.country, a.geo_id, b.is_primary
FROM addresses a
LEFT JOIN company_address b ON a.id = b.address_id 
WHERE b.company_id = company_id;
END