CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_jobs`(
IN c_companyid varchar(60) 
)
BEGIN
Select * From hrgp.company_jobs Where company_id = c_companyid;
END