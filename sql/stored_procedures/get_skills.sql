CREATE DEFINER=`root`@`localhost` PROCEDURE `get_skills`(
IN u_user_id varchar(255)
)
BEGIN

    SELECT * FROM candidate_skills WHERE user_id = u_user_id;

END