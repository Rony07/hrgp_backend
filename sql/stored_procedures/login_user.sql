CREATE DEFINER=`root`@`localhost` PROCEDURE `login_user`(
IN u_user varchar(255)
)
BEGIN
    IF (SELECT count(*) FROM users WHERE email = u_user)>0 THEN
    BEGIN
    
       SELECT a.user_id, a.email, a.date_created, a.user_role_id, a.signup_type, a.password,
       b.first_name, b.last_name, b.image,
       b.phone_no, b.alternate_no , b.about_me, c.address_1, c.address_2, c.city, c.state,
       c.pincode, c.country, c.geo_id, d.address_1 as permanent_address_1, d.address_2 as permanent_address_2, 
       d.city as permanent_city, d.state as permanent_state,d.pincode as permanent_pincode, d.country as permanent_country,
       d.geo_id as permanent_geo_id
       FROM users a
       LEFT JOIN candidate_details b ON a.user_id = b.user_id
       LEFT JOIN addresses c ON b.current_address = c.id
       LEFT JOIN addresses d ON b.permanent_address = d.id
       WHERE email = u_user;
    END;
    ELSE
	    BEGIN
         IF (SELECT count(*) FROM candidate_details WHERE phone_no = u_user)>0 THEN
         BEGIN
         SELECT a.user_id, a.first_name, a.last_name, a.image,
         a.phone_no, a.alternate_no, a.about_me, b.email, b.date_created, b.user_role_id, b.signup_type, b.password,
         c.address_1, c.address_2, c.city, c.state, c.pincode, c.country, c.geo_id,
         d.address_1 as permanent_address_1, d.address_2 as permanent_address_2, 
         d.city as permanent_city, d.state as permanent_state,d.pincode as permanent_pincode, d.country as permanent_country,
         d.geo_id as permanent_geo_id
		FROM candidate_details a
         LEFT JOIN users b ON a.user_id = b.user_id
         LEFT JOIN addresses c ON a.current_address = c.id
         LEFT JOIN addresses d ON a.permanent_address = d.id
         WHERE phone_no = u_user;
         END;
	    ELSE
          BEGIN
          SELECT "false";
          END;
          END IF;
	END;
	END IF;
END