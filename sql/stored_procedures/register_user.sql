CREATE DEFINER=`root`@`localhost` PROCEDURE `register_user`(
IN u_user_id varchar(255),
IN u_email varchar(255),
IN u_created varchar(300),
IN u_last_logged varchar(300),
IN u_user_role varchar(300),
IN u_active varchar(300),
IN u_pass varchar(300),
IN u_signup_type varchar(300),
IN u_phone varchar(300),
IN u_firstname varchar(300),
IN u_lastname varchar(300),
IN u_image varchar(1000)
)
BEGIN
    IF (SELECT count(*) FROM users WHERE email = u_email)>0 THEN
    BEGIN
    SELECT "false";
    END;
    ELSE
    BEGIN
    INSERT INTO users(user_id, email, date_created, last_logged_in, user_role_id, active, password, signup_type)
    VALUES (u_user_id, u_email, u_created, u_last_logged, u_user_role, u_active, u_pass, u_signup_type);
	IF(u_user_role = 3) THEN
    BEGIN
    INSERT INTO candidate_details(user_id, phone_no, first_name, last_name, image) VALUES (u_user_id, u_phone, u_firstname, u_lastname, u_image);
    END; END IF;
    SELECT "true";
	END;
	END IF;
END